(function() {
	var app = angular.module('angularjs-demo', [ 'ngRoute', 'configModule',
			'navControllers', 'userControllers' , 'userServices', 'restaurantControllers', 'restaurantServices', 'cartServices', 'cartControllers', 'orderControllers', 'orderServices', 'employeeControllers', 'adminControllers','adminServices'])
})();