(function() {

    var adminModule = angular.module('adminControllers', [ 'ngRoute' ]);

    adminModule.config(function($routeProvider) {
        $routeProvider.when('/adminHome', {
            templateUrl: 'app/views/admin/home.html',
            controller: 'AdminController',
            controllerAs: "adminCtrl"
        }).when('/adClients', {
            templateUrl: 'app/views/admin/clients.html',
            controller: 'AdminClientController',
            controllerAs: "adminCtrl"
        }).when('/adRestaurants', {
            templateUrl: 'app/views/admin/restaurants.html',
            controller: 'AdminRestaurantController',
            controllerAs: "adminCtrl"
        }).when('/adProducts', {
            templateUrl: 'app/views/admin/products.html',
            controller: 'AdminProductController',
            controllerAs: "adminCtrl"
        });
    });

    adminModule.controller('AdminController', ['$scope', '$route', '$window', 'UserFactory', 'AdminFactory',
        function($scope, $route, $window, UserFactory, AdminFactory) {

        } ]);

    adminModule.controller('AdminClientController', ['$scope', '$route', '$window', 'UserFactory', 'AdminFactory',
        function($scope, $route, $window, UserFactory, AdminFactory) {
            $scope.users = [];

            $scope.credentials = {
                'firstname' : '',
                'lastname' : '',
                'email' : '',
                'password' : '',
                'telephone' : '',
                'address' : {
                    'city' : '',
                    'postalcode' : '',
                    'street' : '',
                    'number': ''
                }
            };

            let promise = UserFactory.findAll();
            promise.success(function(data) {
                $scope.users = data;
            }).error(function(data, status, header, config) {
                console.log("Error getting user list");
            });


            $scope.addClient = function() {
                if(
                    $scope.credentials.firstname &&
                    $scope.credentials.lastname &&
                    $scope.credentials.email &&
                    $scope.credentials.password &&
                    $scope.credentials.telephone &&
                    $scope.credentials.address.city &&
                    $scope.credentials.address.postalcode &&
                    $scope.credentials.address.street &&
                    $scope.credentials.address.number) {
                    let promise = UserFactory.register(
                        $scope.credentials.firstname,
                        $scope.credentials.lastname,
                        $scope.credentials.email,
                        $scope.credentials.password,
                        $scope.credentials.telephone,
                        $scope.credentials.address.city,
                        $scope.credentials.address.postalcode,
                        $scope.credentials.address.street,
                        $scope.credentials.address.number
                    )
                    promise.success(function(data) {
                        alert("User added!")
                        $route.reload();
                    }).error(function(data, status, header, config) {
                        alert("Error. Please try again.")
                    });
                }
                else {
                    console.log($scope.credentials);
                    alert("Please fill in all the fields!");
                }
            }

            $scope.deleteClient = function(email) {
                AdminFactory.removeByEmail(email).success(function(response) {
                    alert("User deleted!");
                    $route.reload();
                }).error(function(data, status, header, config) {
                    alert("Error!");
                });
            }
        } ]);

    adminModule.controller('AdminRestaurantController', ['$scope', '$route', '$window', 'RestaurantFactory', 'AdminFactory',
        function($scope, $route, $window, RestaurantFactory, AdminFactory) {
            $scope.restaurants = [];
            $scope.products = [];

            let promise = RestaurantFactory.findAll();
            promise.success(function(data) {
                $scope.restaurants = data;
            }).error(function(data, status, header, config) {
                alert(status);
            });

            AdminFactory.findAllProducts().success(function(data) {
                $scope.products = data;
            });

            $scope.remove = function(id) {
                RestaurantFactory.removeRestaurant(id);
                alert("Restaurant deleted!");
                $route.reload();
            }

            $scope.addToRestaurant = function(productId, id) {
                console.log(productId + "," +id);
                let price = document.getElementById(id).innerHTML;
                AdminFactory.addProductToRestaurant(productId, id, price);
            }
        } ]);

    adminModule.controller('AdminProductController', ['$scope', '$route', '$window', 'RestaurantFactory', 'AdminFactory',
        function($scope, $route, $window, RestaurantFactory, AdminFactory) {

        } ]);
})();