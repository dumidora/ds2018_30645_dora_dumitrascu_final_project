(function() {
    var cartsModule = angular.module('cartControllers', ['ngRoute']);

    cartsModule.config(function($routeProvider) {
        $routeProvider.when('/cart', {
            templateUrl : 'app/views/cart/cart.html',
            controller : 'CartController',
            controllerAs : 'cartCtrl'
        })
    });

    cartsModule.controller('CartController', [ '$scope', '$window', '$route', 'CartFactory', 'UserFactory',
        function($scope, $window, $route, CartFactory, UserFactory) {
            $scope.cartList = [];
            let user = UserFactory.currentUser();
            let promise = CartFactory.getCart(user.email);

            promise.success(function(data) {
                $scope.cartList = data;
            }).error(function(data, status, header, config) {

                console.log("Getting cart failed with status code:" + status);
            })

            $scope.checkout = function(orderId) {
                let promise = CartFactory.checkout(orderId);
                promise.success(function(response) {
                    alert("Your order has been placed.");
                    $window.location.href = '#/home';
                }).error(function(data, status, header, config) {
                    switch(status) {
                        case 400:
                            if($scope.cartList === []) {
                                alert("Your cart is empty!");
                            } else {
                                alert("You cannot order from two different restaurants!");
                            }
                            break;
                        case 500: alert("Server error. Please try again."); break;
                        default: break;
                    }
                })
            }

            $scope.removeFromCart = function(productId) {
                CartFactory.removeFromCart(productId)
                    .success(function() {
                        alert("You have successfully removed the product from the cart.");
                        $route.reload();
                    })
            }
        } ]);
})();