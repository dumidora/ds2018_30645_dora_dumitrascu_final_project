(function() {

	var usersModule = angular.module('userControllers', [ 'ngRoute' ]);

	usersModule.config(function($routeProvider) {
		$routeProvider.when('/users', {
			templateUrl : 'app/views/user/user-list.html',
			controller : 'AllUsersController',
			controllerAs : "allUsersCtrl"
		}).when('/user/:id', {
			templateUrl : 'app/views/user/user-details.html',
			controller : 'UserController',
			controllerAs : "userCtrl"
		}).when('/register', {
			templateUrl: 'app/views/user/register.html',
			controller: 'RegisterController',
			controllerAs: 'registerCtrl'
		}).when('/logout', {
			templateUrl: 'app/views/homeview.html',
			controller: 'LogoutController',
			controllerAs: 'logoutCtrl'
		})
	});

	usersModule.controller('LogoutController', [ '$scope', '$window', 'UserFactory',
		function($scope, $window, UserFactory) {
			UserFactory.logout();
		} ]);

	usersModule.controller('AllUsersController', [ '$scope', 'UserFactory',
			function($scope, UserFactory) {
				$scope.users = [];
				let promise = UserFactory.findAll();
				promise.success(function(data) {
					$scope.users = data;
					$scope.currentUser = UserFactory.currentUser();
				}).error(function(data, status, header, config) {
					alert(status);
				});

			} ]);

	usersModule.controller('UserController', [ '$scope', '$routeParams',
			'UserFactory', function($scope, $routeParams, UserFactory) {
				var id = $routeParams.id;
				var promise = UserFactory.findById(id);
				$scope.user = null;
				promise.success(function(data) {
					$scope.user = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);

	usersModule.controller('RegisterController', ['$scope', '$window', 'UserFactory',
		function($scope, $window, UserFactory) {
			$scope.credentials = {
				'firstname' : '',
				'lastname' : '',
				'email' : '',
				'password' : '',
				'telephone' : '',
				'address' : {
					'city' : '',
					'postalcode' : '',
					'street' : '',
					'number': ''
				}
			};
			$scope.register = function() {
				if(
					$scope.credentials.firstname &&
					$scope.credentials.lastname &&
					$scope.credentials.email &&
					$scope.credentials.password &&
					$scope.credentials.telephone &&
					$scope.credentials.address.city &&
					$scope.credentials.address.postalcode &&
					$scope.credentials.address.street &&
					$scope.credentials.address.number) {
					let promise = UserFactory.register(
						$scope.credentials.firstname,
						$scope.credentials.lastname,
						$scope.credentials.email,
						$scope.credentials.password,
						$scope.credentials.telephone,
						$scope.credentials.address.city,
						$scope.credentials.address.postalcode,
						$scope.credentials.address.street,
						$scope.credentials.address.number
					)
					promise.success(function(data) {
						alert("Register successful!")
						$window.location.href = '#/';

					}).error(function(data, status, header, config) {
						alert("Error. Please try again.")
					});
				}
				else {
					alert("Please fill in all the fields!");
				}
			}
		} ]);
})();
