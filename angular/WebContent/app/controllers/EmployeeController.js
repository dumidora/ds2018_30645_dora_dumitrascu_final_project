(function() {

    var employeeModule = angular.module('employeeControllers', [ 'ngRoute' ]);

    employeeModule.config(function($routeProvider) {
        $routeProvider.when('/employeeHome', {
            templateUrl: 'app/views/employee/home.html',
            controller: 'EmployeeController',
            controllerAs: "empCtrl"
        });
    });

    employeeModule.controller('EmployeeController', ['$scope', '$route', '$window', 'UserFactory', 'OrderFactory',
        function($scope, $route, $window, UserFactory, OrderFactory) {
            $scope.orders = [];
            let emp = UserFactory.currentUser();
            let promise = UserFactory.employeeHome(emp.username);
            promise.success(function(data) {
                $scope.orders = data;
            }).error(function(data, status, header, config) {
                alert(status);
            });

            $scope.confirmOrder = function(orderId) {
                OrderFactory.confirm(orderId);
                alert("Order confirmed!");
                $route.reload();
            }

        } ]);
})();