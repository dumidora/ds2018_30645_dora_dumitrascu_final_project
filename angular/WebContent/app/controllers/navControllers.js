(function() {

	var homeModule = angular.module('navControllers', [ 'ngRoute' ])

	homeModule.config(function($routeProvider) {
		$routeProvider.when('/', {
			templateUrl : 'app/views/homeview.html',
			controller : 'HomeController',
			controllerAs : "homeCtrl"
		})
	});

	homeModule.controller('HomeController', ['$scope', '$window', 'UserFactory',
		function($scope, $window, UserFactory) {
			$scope.credentials = {
				'email' : '',
				'password' : ''
			};

			$scope.login = function() {
				if($scope.credentials.password && $scope.credentials.email) {
					let promise = UserFactory.login($scope.credentials.email, $scope.credentials.password);
					promise.success(function(data) {
						$scope.currentUser = data;
						console.log("Logged in with " + data.firstname);
						UserFactory.persistUser(data);
						$window.location.href = '#/home'
					}).error(function(data, status, header, config) {
						alert("Invalid credentials!");
					});
				}
				else {
					alert("Please fill in all the fields!");
				}
			}

			$scope.loginEmployee = function(){
				if($scope.credentials.password && $scope.credentials.email) {
					let promise = UserFactory.loginEmployee($scope.credentials.email, $scope.credentials.password);
					promise.success(function(data) {
						$scope.currentUser = data;
						console.log("Logged in with " + data.username);
						UserFactory.persistUser(data);
						$window.location.href = '#/employeeHome';
					}).error(function(data, status, header, config) {
						alert("Invalid credentials!");
					});
				}
				else {
					alert("Please fill in all the fields!");
				}
			};

			$scope.loginAdmin = function(){
				if($scope.credentials.password && $scope.credentials.email) {
					let promise = UserFactory.loginAdmin($scope.credentials.email, $scope.credentials.password);
					promise.success(function(data) {
						$scope.currentUser = data;
						console.log("Logged in with " + data.username);
						UserFactory.persistUser(data);
						$window.location.href = '#/adminHome';
					}).error(function(data, status, header, config) {
						alert("Invalid credentials!");
					});
				}
				else {
					alert("Please fill in all the fields!");
				}
			};

		} ]);

})();