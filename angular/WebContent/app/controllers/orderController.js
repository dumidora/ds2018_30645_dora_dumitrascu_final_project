(function() {
    var orderModule = angular.module('orderControllers', [ 'ngRoute' ]);

    orderModule.config(function($routeProvider) {
        $routeProvider.when('/history', {
            templateUrl : 'app/views/user/history.html',
            controller : 'HistoryController',
            controllerAs : 'historyCtrl'
        })
    });

    // orderModule.controller('HistoryController', [ '$scope', 'OrderFactory', 'UserFactory',
    //     function($scope, OrderFactory, UserFactory) {
    //         UserFactory.logout();
    //     } ]);

    orderModule.controller('HistoryController', ['$scope', 'OrderFactory', 'UserFactory',
        function($scope, OrderFactory, UserFactory) {
            let currentUser = UserFactory.currentUser();
            $scope.orders = [];
            let promise = OrderFactory.getHistory(currentUser.email);
            promise.success(function(data) {
                $scope.orders = data;
            }).error(function(data,status,header,config) {
                alert("Error!");
            });
    }])

})();