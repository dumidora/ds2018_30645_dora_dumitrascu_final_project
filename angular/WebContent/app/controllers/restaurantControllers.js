(function() {

    var restaurantModule = angular.module('restaurantControllers', [ 'ngRoute' ]);

    restaurantModule.config(function($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'app/views/restaurants/restaurants.html',
            controller: 'RestaurantController',
            controllerAs: "restaurantCtrl"
        }).when('/restaurant/:id', {
            templateUrl: 'app/views/restaurants/restaurant-page.html',
            controller: 'RestaurantProductsController',
            controllerAs: 'restaurantProdCtrl'
        });
    });

    restaurantModule.controller('RestaurantController', ['$scope', '$window', 'RestaurantFactory',
        function($scope, $window, RestaurantFactory) {
            $scope.restaurants = [];
            let promise = RestaurantFactory.findAll();
            promise.success(function(data) {
                $scope.restaurants = data;
            }).error(function(data, status, header, config) {
                alert(status);
            });

            $scope.getProducts = function(restaurant) {
                RestaurantFactory.setRestaurant(restaurant)
                $window.location.href = '#/restaurant/' + restaurant.id;
            }
        } ]);
    restaurantModule.controller('RestaurantProductsController', ['$scope', '$routeParams', 'RestaurantFactory', 'UserFactory',
        function($scope, $routeParams, RestaurantFactory, UserFactory) {
            let id = $routeParams.id;
            $scope.products = [];
            $scope.restaurant = RestaurantFactory.getRestaurant();
            var promise = RestaurantFactory.getProducts(id);
            promise.success(function (data) {
                 $scope.products = data;
            }).error(function (data, status, header, config) {
                    alert(status);
            });

            $scope.increase = function(prodId) {
                var i = document.getElementById(prodId).innerHTML;
                i++;
                document.getElementById(prodId).innerHTML = +i;
            }
            $scope.decrease = function(prodId) {
                var i = document.getElementById(prodId).innerHTML;
                if(i>0) {
                    i--;
                    document.getElementById(prodId).innerHTML = +i;
                }
            }

            $scope.addToCart = function(product) {
                var quantity = document.getElementById(product.product.id).innerHTML;

                if(quantity != 0) {
                    var currentUser = UserFactory.currentUser();
                    //if user is logged in
                    var restaurant = RestaurantFactory.getRestaurant()
                    if (currentUser != null) {
                        var promise = RestaurantFactory.addToCart(currentUser.email, quantity, restaurant.name, product.product.id);
                        promise.success(function (data) {
                            alert("Product added to cart")
                        }).error(function (data, status, header, config) {
                            alert("failed with " + status);
                        });
                    } else {
                        alert("User not logged in!");
                    }
                } else {
                    alert("Please specify quantity!");
                }
            }
        }
    ])
})();