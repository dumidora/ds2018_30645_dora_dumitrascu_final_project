(function() {
	var userServices = angular.module('userServices', []);

	userServices.factory('UserFactory', [ '$http', 'config',
			function($http, config) {

				var currentUser = null;

				var saveUser = function(user) {
					currentUser = user;
				}

				var getCurrentUser = function() {
					return currentUser;
				}

				var privateUserDetails = function(id) {
					return $http.get(config.API_URL + '/user/details/' + id);
				};

				var privateUserList = function() {
					return $http.get(config.API_URL + '/client/users');
				};

				var logout = function() {
				    currentUser = null;
                }

				var loginUser = function(email, password) {
					let requestBody = {
						email: email,
						password: password
					};
					return $http.post(config.API_URL + '/client/login', JSON.stringify(requestBody));
				};

				var employee = function(email,password) {
					let requestBody = {
						email: email,
						password: password
					};
					return $http.post(config.API_URL + '/employee/login', JSON.stringify(requestBody));
				};

				var employeeHome = function(username) {
					return $http.get(config.API_URL + '/employee/home/?username=' + username);
				}

				var admin = function(email,password) {
					let requestBody = {
						email: email,
						password: password
					};
					return $http.post(config.API_URL + '/admin/login', JSON.stringify(requestBody));
				};

				var registerUser = function(firstname, lastname, email, password, telephone, city, postalcode, street, number) {
					var requestBody = {
						firstname: firstname,
						lastname: lastname,
						email: email,
						password: password,
						telephone: telephone,
						address: {
							city: city,
							postalcode: postalcode,
							street: street,
							number: number
						}
					};
					return $http.post(config.API_URL + '/client/register', JSON.stringify(requestBody));
				};

				return {
					findById : function(id) {
						return privateUserDetails(id);
					},

					findAll : function() {
						return privateUserList();
					},

					login : function(email, password) {
						return loginUser(email, password);
					},

					register: function(firstname, lastname, email, password, telephone, city, postalcode, street, number) {
						return registerUser(firstname, lastname, email, password, telephone, city, postalcode, street, number);
					},

					currentUser: function(){
						return getCurrentUser();
					},

					persistUser: function(user) {
						saveUser(user);
					},

                    logout: function () {
                        logout();
                    },

					loginEmployee: function(email, password) {
						return employee(email,password);
					},

					loginAdmin: function(email,password) {
						return admin(email,password);
					},

					employeeHome: function(username) {
						return employeeHome(username);
					}
				};
			} ]);

})();