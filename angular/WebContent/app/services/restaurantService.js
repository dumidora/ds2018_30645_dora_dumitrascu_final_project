(function() {
    var restaurantServices = angular.module('restaurantServices', []);

    restaurantServices.factory('RestaurantFactory', [ '$http', 'config',
        function($http, config) {

            var chosenRestaurant = null;

            var chooseRestaurant = function(restaurant) {
                chosenRestaurant = restaurant;
            }

            var getChosenRestaurant = function() {
                return chosenRestaurant;
            }

            var privateRestaurantList = function() {
                return $http.get(config.API_URL + '/client/home');
            };

            var privateRestaurantProducts = function(id) {
                return $http.get(config.API_URL + '/restaurant/productsWithPrice/' + id);
            }

            var privateAddToCart = function(email, quantity, restaurantName, productId) {
                let requestBody = {email:email,quantity:quantity,restaurantName:restaurantName,productId:productId}
                return $http.post(config.API_URL + '/cart/addToCart',requestBody);
            }

            var removeById = function(id) {
                return $http.post(config.API_URL + '/admin/restaurant/delete/' + id);
            }

            return {
                findAll : function() {
                    return privateRestaurantList();
                },

                getProducts : function(id) {
                    return privateRestaurantProducts(id);
                },

                getRestaurant : function() {
                    return getChosenRestaurant();
                },

                setRestaurant : function(restaurant) {
                    chooseRestaurant(restaurant);
                },

                addToCart : function(email, quantity, restaurantName, productId) {
                    return privateAddToCart(email, quantity, restaurantName, productId);
                },

                removeRestaurant : function(id) {
                    return removeById(id);
                }
            };
        } ]);

})();