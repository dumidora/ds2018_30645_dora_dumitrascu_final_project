(function() {
    var adminServices = angular.module('adminServices', []);

    adminServices.factory('AdminFactory', [ '$http', 'config',
        function($http, config) {

            var remove = function(email) {
                return $http.post(config.API_URL + '/admin/client/delete/' + email);
            };

            let getAllProducts = function() {
                return $http.get(config.API_URL + '/admin/products/all');
            };

            let addProd = function(productId, restaurantId, price) {
                let requestBody = {
                    productId: productId,
                    restaurantId: restaurantId,
                    price: price
                };
                console.log(JSON.stringify(requestBody))
                return $http.post(config.API_URL + '/admin/addProduct', JSON.stringify(requestBody));

            }

            return {
                removeByEmail : function(email) {
                    return remove(email);
                },

                findAllProducts : function() {
                    return getAllProducts();
                },

                addProductToRestaurant : function(productId, restaurantId, price) {
                    return addProd(productId, restaurantId, price);
                }
            };
        } ]);

})();