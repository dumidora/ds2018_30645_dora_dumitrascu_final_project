(function() {
    var cartServices = angular.module('cartServices',[]);
    cartServices.factory('CartFactory', [ '$http', 'config',
        function($http, config) {

            let getCartProducts = function(data) {
                return $http.get(config.API_URL + '/cart/' + data);
            };

            let checkoutOrder = function(id) {
                return $http.post(config.API_URL + '/order/checkout?orderId=' + id);
            };

            let remove = function(id) {
                return $http.post(config.API_URL + '/cart/remove?productId=' + id);
            };


            return {
                getCart : function(email) {
                    return getCartProducts(email);
                },

                checkout : function(orderId) {
                    return checkoutOrder(orderId);
                },

                removeFromCart : function(productId) {
                    return remove(productId);
                }
            };
        } ]);
})();