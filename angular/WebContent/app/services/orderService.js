(function() {
    var orderServices = angular.module('orderServices',[]);
    orderServices.factory('OrderFactory', [ '$http', 'config',
        function($http, config) {

            let getHistory = function(data) {
                return $http.get(config.API_URL + '/order/history/' + data);
            };

            let confirmOrder = function(orderId) {
                return $http.post(config.API_URL + '/employee/confirmOrder/' + orderId);
            };

            return {
                getHistory : function(email) {
                    return getHistory(email);
                },

                confirm : function(orderId) {
                    return confirmOrder(orderId);
                }
            };
        } ]);
})();