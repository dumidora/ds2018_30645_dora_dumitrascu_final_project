package com.test.online.market.OnlineMarket.services;

import com.test.online.market.OnlineMarket.model.Address;
import com.test.online.market.OnlineMarket.model.Client;
import com.test.online.market.OnlineMarket.model.Order;
import com.test.online.market.OnlineMarket.repositories.AddressRepository;
import com.test.online.market.OnlineMarket.repositories.ClientRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Transient;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Dora on November, 2018
 */
public class ClientServiceTest {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private AddressRepository addressRepository;

    @Test
    public void getByUsername() {
    }

    @Test
    @Transient
    public void insert() {
        Integer lastId = clientRepository.getMaxId();
        Integer newId = lastId++;
        Address address = new Address("test_city","test_postalcode","test_street",1);
        Integer addressId = addressRepository.save(address).getId();
        address.setId(addressId);
        Client client = new Client("test_firstname","test_lastname","test"+newId+"@yahoo.com","testpass","0712123456",address);
        clientRepository.save(client);
        //assertEquals(lastId++,clientRepository.getMaxId());
        List<Address> addressList = addressRepository.findAll();
        assert(addressList!=null);
    }
}