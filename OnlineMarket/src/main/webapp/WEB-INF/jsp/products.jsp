<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>"${restaurantName}"</title>
</head>

<style>
    .navbar .navbar-brand{
        padding-top: 7px;
    }

    .dropbtn {
        background-color: black;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
        opacity: 0.6;
        transition: 0.3s;
        cursor:pointer;
        border-radius: 12px;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown-content a:hover {background-color: #ddd}

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown:hover .dropbtn {
        opacity:1;
        background-color: #e1e8ae;
        color:black;
    }
</style>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/home/${username}"><img src = "https://drive.google.com/uc?id=11Hcv9NycNECFYPvT4dOsPpVY0bgt65Ki" width = "35" height = "35"/></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/home/${username}">AVOCADO</a></li>
            <li><a href="/cart/${username}">Cart</a></li>
            <li><a href="/history/${username}">History</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/profile/${username}"><span class="glyphicon glyphicon-user"></span>Profile</a></li>
            <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
    </div>
</nav>

<div class = "dropdown">
    <button class = "dropbtn">Filter</button>
    <div class = "dropdown-content">
        <c:forEach items = "${categories}" var = "cat">

            <form action = "/products/${restaurantName}/${cat}">
                <input type = "hidden" value = "${email}" name = "email"/>
                <td><input type="submit" value = ${cat}></td>
            </form>
        </c:forEach>
    </div>
</div>
<table>
    <th>Name</th>
    <th>Description</th>
    <th>Price</th>

    <c:forEach var = "entry" items = "${products}">
        <form action = "/addToCart" method = POST>
            <tr>
            <td>${entry.key.name}</td>
            <td>${entry.key.description}</td>
            <td>${entry.value}</td>
            <input type = "hidden" value = "${entry.key.id}" name = "prodId"/>
            <input type = "hidden" value = "${email}" name = "email"/>
            <input type = "hidden" value = "${restaurantName}" name = "restaurantName"/>
            <td><input type = "text" name = "quantity" placeholder = "Quantity..."/></td>
            <td><input type="submit" value = "Add to cart"/></td>
            </tr>
        </form>
    </c:forEach>
</table>
</body>
</html>