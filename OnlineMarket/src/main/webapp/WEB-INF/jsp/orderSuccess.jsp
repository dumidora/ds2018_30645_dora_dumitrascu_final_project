<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>

</head>
<style>
    body{
        background-color:#00a79d;
    }
    .topnav {
        background-color: #333;
        overflow: hidden;
    }

    /* Style the links inside the navigation bar */
    .topnav a {
        float: left;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 17px;
    }

    /* Change the color of links on hover */
    .topnav a:hover {
        background-color: #ddd;
        color: black;
    }

    /* Add a color to the active/current link */
    .topnav a.active {
        background-color: #82E7D7;
        color: black;
    }
</style>
<head>
</head>
<body>
<div class="topnav">
    <a href="/home/${currentUser.username}">Home</a>
    <a href="/profile/${currentUser.username}">Profile</a>
    <a href="/history/${currentUser.username}">History</a>
    <a href="/logout">Logout</a>
</div>

<p>Your order has been successfully placed!</p>
</body>

</html>

