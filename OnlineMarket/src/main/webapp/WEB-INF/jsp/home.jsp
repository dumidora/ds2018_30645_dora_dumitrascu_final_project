<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<style>
    .myCont{
        float:left;
        width : 480px;
        height: 300px;
        position:relative;

    }
    .myCont img{
        display:block;
        margin:auto;
        width: 250px;
        max-height:100%;
    }
    .navbar .navbar-brand{
        padding-top: 7px;
    }


</style>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Restaurants</title>
</head>
<style>

</style>
<head>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/home/${username}"><img src = "https://drive.google.com/uc?id=11Hcv9NycNECFYPvT4dOsPpVY0bgt65Ki" width = "35" height = "35"/></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/home/${username}">AVOCADO</a></li>
            <li><a href="/cart/${username}">Cart</a></li>
            <li><a href="/history/${username}">History</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/profile/${username}"><span class="glyphicon glyphicon-user"></span>Profile</a></li>
            <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
    </div>
</nav>

<div>
    <h3>restaurants</h3>
    <c:forEach var = "entry" items = "${restaurants}">
        <div class = "myCont">
            <img alt = "img" src = "data:image/jpeg;base64,${entry.value}">
            <div align = "center">
            <p>${entry.key.name}</p>
            <form action = "/products/${entry.key.name}">
                <input type = "hidden" name = "email" value ="${currentUser.email}"/>
                <input type = "hidden" name = "restaurantName" value = "${entry.key.name}">
                <input type = "submit" value = "${entry.key.name}">
            </form>
        </div>

        </div>

    </c:forEach>

</div>


</body>

</html>

