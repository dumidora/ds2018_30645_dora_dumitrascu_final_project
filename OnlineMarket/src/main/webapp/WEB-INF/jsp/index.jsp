<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>EZ_shop</title>
</head>
<style>

    #pcorners{
        border-radius: 25px;
        border: 2px solid #000000;
        padding: 20px;
        width: 350px;
        height: 200px;
        background: rgba(239,239,208,.4);
        position:absolute;
        margin:auto;
        top:0;
        right:0;
        bottom:0;
        left:0;
    }


    #asd{
        border-radius:7px;
        border-width:3px;
        border-style: wave;
        border-color:#C7F6EF;
    }

    #asd2{
        border-radius:7px;
        border-width:3px;
        border-style: wave;
        border-color:#C7F6EF;
    }

    .container{
        display: flex;
        justify-content: center;
    }
    .center{
        width: 380px;
    }

    input[type = button]{
        background-color: black;
        color: white;
        padding: 11px;
        font-size: 16px;
        border: none;
        opacity: 1;
        transition: 0.3s;
        cursor:pointer;
        border-radius: 12px;
    }

    input[type = button]:hover{
        background-color: white;
        color: black
    }

    input[type=submit]{
        background-color: black;
        color: white;
        padding: 11px;
        font-size: 16px;
        border: none;
        transition: 0.3s;
        cursor:pointer;
        border-radius: 12px;
        opacity:1;
    }

    input[type = submit]:hover{
        background-color: white;
        color: black;
    }

    h2{
        display:block;
        font-size: 16px;
        color:black;
    }

    body{
        background-image: url("https://drive.google.com/uc?id=19eBAXo2733GzuGAEXL5b2XCSe4Ifps35");
        background-repeat: no-repeat;
        background-size:cover;
    }

    #footer{
        position : fixed;
        bottom:0;
        width :100%;
        align:center;
    }
</style>


<body>
<div class = "container">
    <div class ="center">
<div id="pcorners" align="center">
    <form action = "/loginPage" method = "POST">
        <input id= "asd" type = "text" name = "email" placeholder="E-mail"/><br/><br/>
        <input id = "asd2" type = "password" name = "password" placeholder="Password"/><br/><br/>
        <input type = "submit" value = "Login"/>
    </form>
    <a href = "/register"><input type = "button" value = "Register"/></a>
</div>
    </div>
</div>


<div id = "footer">
    <a href="/employeeLogin">Sign in with employee</a>
    <a href="/adminLogin">Sign in with admin</a>
</div>


</body>
</html>