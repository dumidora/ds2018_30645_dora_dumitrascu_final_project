<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<html>
<style>
    .topnav {
        background-color: #333;
        overflow: hidden;
    }
    .topnav a {
        float: left;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 17px;
    }
    /* Change the color of links on hover */
    .topnav a:hover {
        background-color: #ddd;
        color: black;
    }
    div{
        border-radius : 5px;
        background-color: rgba(242,242,242,.4);
        padding: 20px;
        width:400px;
    }
    input[type=submit] {
        width: 50%;
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    html{
        background:url("https://drive.google.com/uc?id=1XxJ1oLGxfWL8RevsNh3qA_lrQj-4gaoo") no-repeat center center fixed;
        -webkit-background-size:cover;
        -moz-background-size:cover;
        -o-background-size:cover;
        background-size:cover;
    }

</style>
<head>
<script type = "text/javascript">

    function isEmpty(str){
        return !str.replace(/^\s+/g,'').length; //true if field is empty
    }

    function validateForm(){
        var a = document.forms["myForm"]["firstname"].value;
        var b = document.forms["myForm"]["lastname"].value;
        var c = document.forms["myForm"]["email"].value;
        var d = document.forms["myForm"]["password"].value;
        var e = document.forms["myForm"]["telephone"].value;
        var f = document.forms["myForm"]["city"].value;
        var g = document.forms["myForm"]["street"].value;
        var h = document.forms["myForm"]["postalcode"].value;
        var i = document.forms["myForm"]["number"].value;


        if(a == null || a ==""|| isEmpty(a) ||
           b == null || b ==""|| isEmpty(b) ||
           c == null || c ==""|| isEmpty(c) ||
           d == null || d ==""|| isEmpty(d) ||
           e == null || e ==""|| isEmpty(e) ||
           f == null || f ==""|| isEmpty(f) ||
           g == null || g ==""|| isEmpty(g) ||
           h == null || h ==""|| isEmpty(h) ||
           i == null || i ==""|| isEmpty(i) ){
            alert("Please fill all required fields and be aware of whitespaces!");
            return false;
        }


    }
</script>
</head>
<body>
<div class="topnav">
    <a href="/">Home</a>
</div>
<br/><br/>

<div align= "left">
<form action = "/registered" name = "myForm" method = "POST" onsubmit = "return validateForm()">
    Firstname:<br/> <input type = "text" name = "firstname" placeholder="Firstname"/><br/><br/>
    Lastname:<br/> <input type = "text" name = "lastname" placeholder="Lastname"/><br/><br/>
    Email:<br/> <input type = "email" name = "email" placeholder="E-mail"/><br/><br/>
    Telephone:<br/><input type = "text" name = "telephone" placeholder="Phone no."/><br/><br/>
    Password:<br/> <input type = "password" name = "password" placeholder="Password"/><br/><br/>
    City:<br/> <input type = "text" name = "city" placeholder="City"/><br/><br/>
    Postal code:<br/> <input type = "text" name = "postalcode" placeholder="Postal code"/><br/><br/>
    Street:<br/><input type = "text" name = "street" placeholder="Street"/><br/> <input type = "text" name = "number" placeholder="Number"/><br/><br/>
    <input type = "submit" value = "Register"/>
</form>
</div>
</body>
</html>