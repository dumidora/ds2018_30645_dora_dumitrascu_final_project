<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Error</title>
</head>
<body style = "background-color: #525D76">
<div style="text-align: center;">
    <c:choose>
        <c:when test = "${OK}">
            All good man
        </c:when>
        <c:otherwise>
            <script type = "text/javascript">
                alert("${msg}");
                window.location.href = "${url}";
            </script>
        </c:otherwise>
    </c:choose>
</div>


</body>
</html>