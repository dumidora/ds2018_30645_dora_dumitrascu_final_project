<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<style>

    body{
        background-color:#00a79d;
    }
    .topnav {
        background-color: #333;
        overflow: hidden;
    }

    /* Style the links inside the navigation bar */
    .topnav a {
        float: left;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 17px;
    }

    /* Change the color of links on hover */
    .topnav a:hover {
        background-color: #ddd;
        color: black;
    }

    /* Add a color to the active/current link */
    .topnav a.active {
        background-color: #82E7D7;
        color: black;
    }
    {
        box-sizing: border-box;
    }


    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 300px;
        margin: auto;
        text-align: center;
        font-family: arial;
        background-color: #94B5B0;
    }


</style>
<head>

</head>
<body>
<div class="topnav">
    <a class ="active" href="/sellerHome/${seller.username}">Home</a>
    <a href="/sellerProfile/${seller.username}">Profile</a>
    <a href="/">Logout</a>
</div>
<br/><br/>
<div class ="card">
    <img src = "https://drive.google.com/uc?id=1QbU92GUUs73JHeiVPk8sd8keFqmdmZeY" width= "30px", height ="30px"/><h2>User details</h2>
    <table id ="details" align = "center">
        <tr>
            <td>Firstname:</td>
            <td>${seller.firstname}</td>
        </tr>
        <tr>
            <td>Lastname:</td>
            <td>${seller.lastname}</td>
        </tr>
        <tr>
            <td>Username:</td>
            <td>${seller.username}</td>
        </tr>
        <tr>
            <td>Balance:</td>
            <td>${seller.balance}</td>
        </tr>
        <tr>
            <td>User type:</td>
            <td>${seller.type}</td>
        </tr>
    </table>
</div>

</body>

</html>