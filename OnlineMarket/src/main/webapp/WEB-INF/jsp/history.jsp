<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
    body{
        background-color:#00a79d;
    }

    .navbar .navbar-brand{
        padding-top: 7px;
    }

    #productTable{
        border-collapse: collapse;
        width:70%;
        align: center;
    }

    #productTable td, #productTable th{
        border: 1px solid #ddd;
        padding: 8px;
        background-color: lightblue;
    }

    #productTable tr:nth-child(even){background-color: #f2f2f2;}

    #productTable tr:hover{
        background-color: #627B77;
    }

    #productTable th{
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #7FB8AF;
        color: #0E433B;
    }


</style>
<head>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/home/${username}"><img src = "https://drive.google.com/uc?id=11Hcv9NycNECFYPvT4dOsPpVY0bgt65Ki" width = "35" height = "35"/></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/home/${username}">AVOCADO</a></li>
            <li><a href="/cart/${username}">Cart</a></li>
            <li><a href="/history/${username}">History</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/profile/${username}"><span class="glyphicon glyphicon-user"></span>Profile</a></li>
            <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
    </div>
</nav>

<br/><br/>
<h1>Finished orders</h1>
<table id="productTable">
    <tr>
        <th>Placement date</th>
        <th>Status</th>
        <th>Products ordered</th>
    </tr>
    <c:forEach var="entry" items="${myMap}">

            <tr>
                <td><c:out value="${entry.key.date}"></c:out></td>
                <td><c:out value = "${entry.key.status}"></c:out></td>
                <td>
                    <c:forEach items = "${entry.value}" var = "cart">
                        <p>${cart.product.name}</p>
                    </c:forEach>
                </td>
            </tr>
    </c:forEach>
</table>

</body>

</html>

