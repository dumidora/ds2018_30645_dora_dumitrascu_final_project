<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>

</style>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li class="active"><a href="/employeeHome/${employee.username}">Pending orders</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
    </div>
</nav>

<h1>Pending orders</h1>
For ${employee.username}

<table>
    <th>Order no.</th>
    <th>Items</th>
    <th>Action</th>
<c:forEach items ="${myMap}" var = "entry">
    <form action = "/confirmOrder" method = POST>
        <tr>
            <td>${entry.key.id}</td>
            <td>
                <c:forEach items = "${entry.value}" var = "cart">
                ${cart.product.name}, quantity = ${cart.quantity}<br/>
                </c:forEach>
            </td>
            <td>
                <input type = "submit" value = "Ship order">
                <input type = "hidden" name = "username" value = "${employee.username}">
                <input type = "hidden" name = "orderId" value = "${entry.key.id}">
            </td>
        </tr>
    </form>
</c:forEach>
</table>
</body>
</html>