<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<title>Profile</title>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>

    body{
        background-color:#00a79d;
    }
    .topnav {
        background-color: #333;
        overflow: hidden;
    }

    /* Style the links inside the navigation bar */
    .topnav a {
        float: left;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 17px;
    }

    /* Change the color of links on hover */
    .topnav a:hover {
        background-color: #ddd;
        color: black;
    }

    /* Add a color to the active/current link */
    .topnav a.active {
        background-color: #82E7D7;
        color: black;
    }
    {
        box-sizing: border-box;
    }


    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 300px;
        margin: auto;
        text-align: center;
        font-family: arial;
        background-color: #94B5B0;
    }
    .navbar .navbar-brand{
        padding-top: 7px;
    }
</style>
<head>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/home/${username}"><img src = "https://drive.google.com/uc?id=11Hcv9NycNECFYPvT4dOsPpVY0bgt65Ki" width = "35" height = "35"/></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/home/${username}">AVOCADO</a></li>
            <li><a href="/cart/${username}">Cart</a></li>
            <li><a href="/history/${username}">History</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/profile/${username}"><span class="glyphicon glyphicon-user"></span>Profile</a></li>
            <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
    </div>
</nav>

<div class ="card">
    <img src = "https://drive.google.com/uc?id=1QbU92GUUs73JHeiVPk8sd8keFqmdmZeY" width= "30px", height ="30px"/><h2>User details</h2>
    <table id ="details" align = "center">
        <tr>
            <td>Firstname:</td>
            <td>${currentUser.firstname}</td>
        </tr>
        <tr>
            <td>Lastname:</td>
            <td>${currentUser.lastname}</td>
        </tr>
        <tr>
            <td>Username:</td>
            <td>${username}</td>
        </tr>
        <tr>
            <td>E-mail:</td>
            <td>${currentUser.email}</td>
        </tr>
        <tr>
            <td>Address:</td>
            <td>
                <p>City: ${currentUser.address.city}</p>
                <p>Postal code: ${currentUser.address.postalcode}</p>
                <p>Street: ${currentUser.address.street}</p>
                <p>Number ${currentUser.address.number}</p>
            </td>
        </tr>
    </table>
</div>

</body>
</html>