<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<style>
    .navbar .navbar-brand{
        padding-top: 7px;
    }
</style>

<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/home/${username}"><img src = "https://drive.google.com/uc?id=11Hcv9NycNECFYPvT4dOsPpVY0bgt65Ki" width = "35" height = "35"/></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/home/${username}">AVOCADO</a></li>
            <li><a href="/cart/${username}">Cart</a></li>
            <li><a href="/history/${username}">History</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/profile/${username}"><span class="glyphicon glyphicon-user"></span>Profile</a></li>
            <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
    </div>
</nav>

<table id = "prodTable">
    <th>Product</th>
    <th>Quantity</th>
    <th>Price</th>
    <th>Action</th>
    <c:forEach var = "entry" items = "${cartList}">
        <form action = "/removeFromCart" method = POST>
            <tr>
                <td>${entry.key.product.name}</td>
                <td>${entry.key.quantity}</td>
                <td>${entry.value}</td>
                <input type = "hidden" value = "${username}" name = "username"/>
                <input type = "hidden" value = "${restaurantName}" name = "restaurantName"/>
                <input type = "hidden" value = "${entry.key.product.id}" name = "prodId"/>
                <td><input type="submit" value = "Remove from cart"/></td>
            </tr>
        </form>
    </c:forEach>
</table>

<div>
    <p>OrderID = ${orderId}</p>
    <p>Total price = ${total}</p>

    <form action = "/orderValidation" method = "POST">
        <input type = "hidden" name = "orderId" value = "${orderId}"/>
        <input type = "hidden" name = "username" value = "${username}"/>
        <input type = "hidden" name = "total" value = "${total}"/>
        <input class= "button1 subButton" type ="submit" value = "Submit order"/>
    </form>
</div>
</body>
</html>

