package com.test.online.market.OnlineMarket.dto;

import com.test.online.market.OnlineMarket.model.Employee;
import com.test.online.market.OnlineMarket.model.MenuItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantDTO {
    private Integer id;
    private String name;
    private Employee employee;
    private List<MenuItem> menuItems;
    private byte[] image;

    public static class Builder{
        private Integer idBuilder;
        private String nameBuilder;
        private Employee employeeBuilder;
        private List<MenuItem> menuBuilder;
        private byte[] imageBuilder;

        public Builder setId(Integer idBuilder){
            this.idBuilder = idBuilder;
            return this;
        }

        public Builder setName(String nameBuilder){
            this.nameBuilder = nameBuilder;
            return this;
        }

        public Builder setEmployee(Employee employeeBuilder){
            this.employeeBuilder = employeeBuilder;
            return this;
        }

        public Builder setMenuItems(List<MenuItem> menuBuilder){
            this.menuBuilder = menuBuilder;
            return this;
        }

        public Builder setImage(byte[] imageBuilder){
            this.imageBuilder = imageBuilder;
            return this;
        }

        public RestaurantDTO create(){
            return new RestaurantDTO(idBuilder,nameBuilder,employeeBuilder,menuBuilder,imageBuilder);
        }
    }

}
