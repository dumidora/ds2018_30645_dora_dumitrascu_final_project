package com.test.online.market.OnlineMarket.dto;

/**
 * Created by Dumitrascu Dora on Dec, 2018
 */
public class RestaurantProductDTO {
    private ProductDTO product;
    private Float price;

    public RestaurantProductDTO(ProductDTO product, Float price) {
        this.product = product;
        this.price = price;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
