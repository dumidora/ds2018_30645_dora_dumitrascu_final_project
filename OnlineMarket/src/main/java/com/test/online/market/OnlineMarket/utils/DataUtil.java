package com.test.online.market.OnlineMarket.utils;

import com.test.online.market.OnlineMarket.dto.ProductDTO;
import com.test.online.market.OnlineMarket.dto.RestaurantDTO;
import com.test.online.market.OnlineMarket.model.Restaurant;
import org.apache.tomcat.util.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Dora on November, 2018
 */
public class DataUtil {

    public static String convertEmail(String email){
        return email.split("@")[0];
    }

    public static Map<RestaurantDTO,String> convertImages(ArrayList<RestaurantDTO> restaurants){
        Map<RestaurantDTO, String> result = new HashMap<>();
        byte[] image;
        ArrayList<String> convertedImages = new ArrayList<>();

        for(RestaurantDTO restaurant: restaurants){
            image = restaurant.getImage();
            byte[] encode = Base64.encodeBase64(image);
            String base64encoded = null;
            try {
                base64encoded = new String(encode,"UTF-8");
                result.put(restaurant,base64encoded);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Map<ProductDTO,Float> filterByCategory(Map<ProductDTO,Float> map, String category){
        return map.entrySet()
                .stream()
                .filter(e->e.getKey().getCategory().toString().equals(category))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

}
