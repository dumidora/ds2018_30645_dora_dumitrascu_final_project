package com.test.online.market.OnlineMarket.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

/**
 * Created by Dora on November, 2018
 */

@Entity
@Table(name = "address")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "city", nullable = false, length = 50)
    private String city;

    @Column(name = "postalcode", nullable = false, length = 6)
    private String postalcode;

    @Column(name = "street", nullable = false, length = 100)
    private String street;

    @Column(name = "number", nullable = false)
    private Integer number;


    public Address(String city, String postalcode, String street, Integer number) {
        this.city = city;
        this.postalcode = postalcode;
        this.street = street;
        this.number = number;
    }

    @JsonIgnore
    @OneToOne(fetch  = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "address")
    private Client client;
}
