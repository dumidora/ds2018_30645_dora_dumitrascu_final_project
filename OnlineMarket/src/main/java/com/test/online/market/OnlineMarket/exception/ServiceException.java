package com.test.online.market.OnlineMarket.exception;

/**
 * Created by Dora on November, 2018
 */
public class ServiceException extends Exception{
    public ServiceException(){super();}
    public ServiceException(String message){super(message);}
}
