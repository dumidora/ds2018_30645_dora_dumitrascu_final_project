package com.test.online.market.OnlineMarket.utils;

/**
 * Created by Dora on November, 2018
 */
public enum OrderStatus {
    PENDING,SENT,SHIPPING,DELIVERED
}
