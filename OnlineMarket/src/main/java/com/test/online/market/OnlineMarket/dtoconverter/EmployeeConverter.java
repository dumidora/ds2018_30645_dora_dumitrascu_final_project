package com.test.online.market.OnlineMarket.dtoconverter;

import com.test.online.market.OnlineMarket.dto.EmployeeDTO;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Employee;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@Component
public class EmployeeConverter implements Converter<Employee,EmployeeDTO>{
    @Override
    public EmployeeDTO toDTO(Employee object) throws ConverterException {
        if(object!=null)
            return new EmployeeDTO.Builder()
                .setUsername(object.getUsername())
                .setPassword(object.getPassword())
                .setRestaurant(object.getRestaurant())
                .create();
        else
            throw new ConverterException("Employee not in database!");
    }

    @Override
    public List<EmployeeDTO> toDTOs(List<Employee> objects) throws ConverterException {
        List<EmployeeDTO> result = new ArrayList<>();
        for(Employee employee : objects){
            result.add(toDTO(employee));
        }
        return result;
    }

    @Override
    public Employee toEntity(EmployeeDTO object) {
        return null;
    }

    @Override
    public List<Employee> toEntities(List<EmployeeDTO> objects) {
        return null;
    }
}
