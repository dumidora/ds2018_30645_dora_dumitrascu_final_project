package com.test.online.market.OnlineMarket.controller;

import com.test.online.market.OnlineMarket.dto.ProductDTO;
import com.test.online.market.OnlineMarket.dto.RestaurantProductDTO;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Product;
import com.test.online.market.OnlineMarket.services.OrderService;
import com.test.online.market.OnlineMarket.services.ProductService;
import com.test.online.market.OnlineMarket.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
@RestController
@RequestMapping({"/api/restaurant"})
public class RestaurantController {

    @Autowired
    ProductService productService;
    RestaurantService restaurantService;
    OrderService orderService;

    @Autowired
    public RestaurantController(RestaurantService restaurantService, ProductService productService, OrderService orderService) {
        this.restaurantService = restaurantService;
        this.productService = productService;
        this.orderService = orderService;
    }
    
    @GetMapping("/products/{restaurantId}")
    public Collection<Product> listProductsForRestaurant(@PathVariable(name = "restaurantId") Integer id) throws ConverterException {
        return productService.getProdForRest(id);
    }

    @GetMapping("/productsWithPrice/{restaurantId}")
    public List<RestaurantProductDTO> prods(@PathVariable(name = "restaurantId") Integer id) throws ConverterException {
        return productService.getProductsForRestaurant(id);
    }
}
