package com.test.online.market.OnlineMarket.repositories;

import com.test.online.market.OnlineMarket.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
public interface ClientRepository extends JpaRepository<Client,Integer>{
    Client findByEmailAndPassword(String email,String password);
    Client findByEmail(String email);

    @Transactional
    void deleteByEmail(String email);

    /**
     * Query to select a client based on the generated username (which is the string
     * in the e-mail before the '@' sign.
     */
    @Query("SELECT c FROM Client c WHERE (LOWER(c.email) like :username)")
    Client findClientByUsername_Generated(@Param("username") String username);

    @Query("SELECT MAX(c.id) FROM Client c")
    Integer getMaxId();

}
