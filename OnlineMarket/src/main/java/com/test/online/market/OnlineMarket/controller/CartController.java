package com.test.online.market.OnlineMarket.controller;

import com.test.online.market.OnlineMarket.dto.*;
import com.test.online.market.OnlineMarket.exception.ControllerException;
import com.test.online.market.OnlineMarket.exception.ConverterException;

import com.test.online.market.OnlineMarket.services.*;
import com.test.online.market.OnlineMarket.utils.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@CrossOrigin(origins = "http://localhost:63342")
@RestController
@RequestMapping({"/api/cart"})
public class CartController {

    private ClientService clientService;
    private OrderService orderService;
    private CartService cartService;
    private RestaurantService restaurantService;
    private ProductService productService;


    @Autowired
    public CartController(ClientService clientService, OrderService orderService, CartService cartService, RestaurantService restaurantService, ProductService productService) {
        this.clientService = clientService;
        this.orderService = orderService;
        this.cartService = cartService;
        this.restaurantService = restaurantService;
        this.productService = productService;
    }


    @PostMapping("/addToCart")
    public ResponseEntity<String> addToCart(@RequestBody CartProductDTO cart) {

        try {
            //set user that is placing the order
            ClientDTO client = clientService.getByEmail(cart.getEmail());

            //get the last inserted order to be able to create a new order
            Integer lastId = orderService.getMaxId();
            //check if quantity field is valid
            if (cart.getQuantity() == null) {
                throw new ControllerException("Invalid field!");
            }
            //if no order is present in the database
            if (lastId == null) {
                lastId = 1;
            }
            RestaurantDTO restaurantDTO = restaurantService.findByName(cart.getRestaurantName());
            cartService.insertCartEntry(lastId, cart.getProductId(), cart.getQuantity(), restaurantDTO.getId());

        } catch (ControllerException | ConverterException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{email}")
    public ResponseEntity<CartListDTO> myCart(@PathVariable String email) {
        try {
            ClientDTO clientDTO = clientService.getByEmail(email);
            Integer orderId = orderService.getMaxId();
            OrderDTO orderDTO = orderService.findById(orderId);

            List<CartDTO> carts = cartService.getCartByOrder(orderId);

            if (orderDTO.getStatus().equals(OrderStatus.SENT)) {
                orderService.createInitialOrder(clientDTO);
            } else {
               carts = cartService.getCartProducts(carts);
            }

            if(carts.isEmpty())
                return new ResponseEntity<>(new CartListDTO(carts, Float.valueOf(0)), HttpStatus.OK);

            CartListDTO cartList = new CartListDTO(carts, cartService.getTotalPrice(carts));
            return new ResponseEntity<>(cartList, HttpStatus.OK);
        } catch (ConverterException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/remove")
    public ResponseEntity removeFromCart(Integer productId) {
        try {
            ProductDTO productDTO = productService.findById(productId);
            cartService.removeFromCart(productDTO, orderService.getMaxId());
        } catch (ConverterException e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}
