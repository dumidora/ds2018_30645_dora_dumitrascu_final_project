package com.test.online.market.OnlineMarket.services;

import com.test.online.market.OnlineMarket.dto.CartDTO;
import com.test.online.market.OnlineMarket.dto.OrderDTO;
import com.test.online.market.OnlineMarket.dto.ProductDTO;
import com.test.online.market.OnlineMarket.dtoconverter.CartConverter;
import com.test.online.market.OnlineMarket.dtoconverter.OrderConverter;
import com.test.online.market.OnlineMarket.dtoconverter.ProductConverter;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Cart;
import com.test.online.market.OnlineMarket.model.Order;
import com.test.online.market.OnlineMarket.model.Product;
import com.test.online.market.OnlineMarket.model.Restaurant;
import com.test.online.market.OnlineMarket.repositories.CartRepository;
import com.test.online.market.OnlineMarket.repositories.OrderRepository;
import com.test.online.market.OnlineMarket.repositories.ProductRepository;
import com.test.online.market.OnlineMarket.repositories.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@Service
public class CartService {
    private CartRepository cartRepository;
    private CartConverter cartConverter;
    private OrderRepository orderRepository;
    private OrderConverter orderConverter;
    private ProductRepository productRepository;
    private ProductConverter productConverter;
    private RestaurantRepository restaurantRepository;


    public CartService(){}
    @Autowired
    public CartService(CartRepository cartRepository, CartConverter cartConverter, OrderRepository orderRepository, ProductRepository productRepository, ProductConverter productConverter, OrderConverter orderConverter, RestaurantRepository restaurantRepository) {
        this.cartRepository = cartRepository;
        this.cartConverter = cartConverter;
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.productConverter = productConverter;
        this.orderConverter = orderConverter;
        this.restaurantRepository = restaurantRepository;
    }

    public void insertCartEntry(Integer orderId, Integer productId, Integer quantity, Integer restaurantId){
        Order order = orderRepository.findById(orderId).get();
        Product product = productRepository.findById(productId).get();
        Restaurant restaurant = restaurantRepository.findById(restaurantId).get();
        List<Cart> currentCart = cartRepository.findByOrder(order);

        for(Cart cart: currentCart) {
            if(cart.getProduct().getId() == product.getId()) {
                cartRepository.updateStatus(cart.getQuantity() + quantity, orderId, productId);
                return;
            }
        }

        Cart cart = new Cart(quantity,product,order, restaurant);
        cartRepository.save(cart);
    }

    public List<CartDTO> getCartByOrder(Integer orderId) throws ConverterException {
        List<CartDTO> carts;
        //get order
        Order order = orderRepository.findById(orderId).get();
        carts = cartConverter.toDTOs(cartRepository.findByOrder(order));
        return carts;
    }

    public List<CartDTO> findAll() throws ConverterException {
        return cartConverter.toDTOs(cartRepository.findAll());
    }


    public Float getTotalPrice(List<CartDTO> carts){
        Float totalPrice = 0f;
        Float productPrice;
        //get price for each product from the given restaurant's menu!
        for(CartDTO cart: carts){
            //get price
            Integer productId = cart.getProduct().getId();
            Integer restaurantId = cart.getRestaurant().getId();

            productPrice = productRepository.getProductPrice(productId,restaurantId).get(0);
            //add to total
            totalPrice= totalPrice+ cart.getQuantity()*productPrice;
        }

        return totalPrice;
    }

    /**
     * method that maps the price for each product in a cart
     * we need this method because one product can have different prices at different restaurants
     * @param carts - the list of the products from the current cart
     * @return
     */
    public List<CartDTO> getCartProducts(List<CartDTO> carts) {
        List<CartDTO> result = new ArrayList<>();
        for(CartDTO cart: carts) {
            Integer productId = cart.getProduct().getId();
            Integer restaurantId = cart.getRestaurant().getId();
            Float productPrice = productRepository.getProductPrice(productId, restaurantId).get(0);
            result.add(
                    new CartDTO.Builder()
                            .setOrder(cart.getOrder())
                            .setProduct(cart.getProduct())
                            .setQuantity(cart.getQuantity())
                            .setRestaurant(cart.getRestaurant())
                            .setPrice(productPrice)
                            .create());
        }
        return result;
    }

    public void removeFromCart(ProductDTO product, Integer orderId){
        cartRepository.deleteByProductAndOrder(productConverter.toEntity(product),orderRepository.findById(orderId).get());
    }

    /**
     * method to check that a user doesn't want to order from multiple restaurants
     * ONE RESTAURANT PER ORDER
     */
    public boolean isCartValid(Integer orderId) throws ConverterException {
        List<CartDTO> cartDTOS = getCartByOrder(orderId);
        Integer restaurantId = cartDTOS.get(0).getRestaurant().getId();
        for(CartDTO cartDTO : cartDTOS){
            if(cartDTO.getRestaurant().getId()!=restaurantId)
                return false;
        }
        return true;
    }



}
