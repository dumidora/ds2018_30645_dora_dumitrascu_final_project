package com.test.online.market.OnlineMarket.services;

import com.test.online.market.OnlineMarket.dto.AddressDTO;
import com.test.online.market.OnlineMarket.dto.ClientDTO;
import com.test.online.market.OnlineMarket.dtoconverter.AddressConverter;
import com.test.online.market.OnlineMarket.dtoconverter.ClientConverter;
import com.test.online.market.OnlineMarket.model.Address;
import com.test.online.market.OnlineMarket.model.Client;
import com.test.online.market.OnlineMarket.repositories.AddressRepository;
import com.test.online.market.OnlineMarket.repositories.ClientRepository;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@Service
public class ClientService {
    private ClientRepository clientRepository;
    private ClientConverter clientConverter;
    private AddressRepository addressRepository;
    private AddressConverter addressConverter;

    public ClientService(){}

    @Autowired
    public ClientService(ClientRepository clientRepository, ClientConverter clientConverter, AddressRepository addressRepository, AddressConverter addressConverter) {
        this.clientRepository = clientRepository;
        this.clientConverter = clientConverter;
        this.addressConverter = addressConverter;
        this.addressRepository = addressRepository;
    }

    public List<Client> getAllModels() {
        return clientRepository.findAll();
    }
    
    public List<ClientDTO> getAll(){
        List<Client> clients;
        List<ClientDTO> clientDTOS = null;
        
        clients = clientRepository.findAll();
        try{
            clientDTOS = clientConverter.toDTOs(clients);
        }
        catch(ConverterException exception){
            exception.printStackTrace();
        }
        return clientDTOS;
    }
    
    public ClientDTO login(String email, String password) throws ConverterException {
        return clientConverter.toDTO(clientRepository.findByEmailAndPassword(email,password));
    }

    public ClientDTO getByEmail(String email){
        Client client = clientRepository.findByEmail(email);
        ClientDTO clientDTO = null;
        try{
            clientDTO = clientConverter.toDTO(client);
        }
        catch(ConverterException exception){
            exception.printStackTrace();
        }
        return clientDTO;
    }

    public ClientDTO getByUsername(String username){
        Client client = clientRepository.findClientByUsername_Generated(username+"@%");
        ClientDTO clientDTO = null;
        try{
            clientDTO = clientConverter.toDTO(client);
        }
        catch(ConverterException e){
            e.printStackTrace();
        }
        return clientDTO;
    }

    public void deleteByEmail(String email) {
        clientRepository.deleteByEmail(email);
    }

    public void insert(String firstname, String lastname, String email, String password, String telephone, Integer addressId){
        //get Address
        Address address =addressRepository.findById(addressId).get();
        ClientDTO clientDTO = new ClientDTO.Builder()
                .setFirstname(firstname)
                .setLastname(lastname)
                .setEmail(email)
                .setPassword(password)
                .setTelephone(telephone)
                .setAddress(address)
                .create();
        clientRepository.save(clientConverter.toEntity(clientDTO));
    }

    public Integer getMaxId(){
        Integer a = clientRepository.getMaxId();
        return a;
    }
}
