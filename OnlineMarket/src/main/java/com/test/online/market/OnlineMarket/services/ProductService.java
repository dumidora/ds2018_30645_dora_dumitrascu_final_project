package com.test.online.market.OnlineMarket.services;

import com.test.online.market.OnlineMarket.dto.ProductDTO;
import com.test.online.market.OnlineMarket.dto.ProductDTOBox;
import com.test.online.market.OnlineMarket.dto.RestaurantProductDTO;
import com.test.online.market.OnlineMarket.dtoconverter.ProductConverter;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Product;
import com.test.online.market.OnlineMarket.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dora on November, 2018
 */

@Service
public class ProductService {

    private ProductRepository productRepository;
    private ProductConverter productConverter;

    @Autowired
    public ProductService(ProductRepository productRepository, ProductConverter productConverter) {
        this.productRepository = productRepository;
        this.productConverter = productConverter;
    }

    public List<RestaurantProductDTO> getProductsForRestaurant(Integer restaurantId) throws ConverterException {
        List<Object[]> objects = productRepository.getRestaurantProducts(restaurantId);

        //iterator -> gather products so that we do not have to go through the object array
        ProductDTOBox box = new ProductDTOBox();
        for(Object[] objects1 : objects) {
            box.add(productConverter.toDTO((Product)objects1[0]));
        }
        ProductDTOBox.Iterator iterator = box.getIterator();

        iterator.first();
        HashMap<ProductDTO,Float> myMap = new HashMap<>();

        for(Object[] objects1 : objects){
            ProductDTO productDTO = iterator.currentValue();
            myMap.put(productDTO,((Float)objects1[1]));
            iterator.next();
        }

        List<RestaurantProductDTO> result = new ArrayList<>();

        for(Map.Entry<ProductDTO, Float> entry: myMap.entrySet()) {
            result.add(new RestaurantProductDTO(entry.getKey(), entry.getValue()));
        }

        return result;
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public List<Product> getProdForRest(Integer restaurantId) throws ConverterException {
        List<Product> list = new ArrayList<Product>();
        List<RestaurantProductDTO> map = getProductsForRestaurant(restaurantId);
        for(RestaurantProductDTO productDTO : map) {
            list.add(productConverter.toEntity(productDTO.getProduct()));
        }
        return list;
    }

    public ProductDTO findById(Integer productId) throws ConverterException {
        return productConverter.toDTO(productRepository.findById(productId).get());
    }

    public List<ProductDTO> getAll() throws ConverterException {
        return productConverter.toDTOs(productRepository.findAll());
    }

    public List<Product> getAllModels() {
        return productRepository.findAll();
    }
}
