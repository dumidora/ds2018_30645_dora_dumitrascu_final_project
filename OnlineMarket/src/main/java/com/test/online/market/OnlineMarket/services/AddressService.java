package com.test.online.market.OnlineMarket.services;

import com.test.online.market.OnlineMarket.dto.AddressDTO;
import com.test.online.market.OnlineMarket.dtoconverter.AddressConverter;
import com.test.online.market.OnlineMarket.repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Dora on November, 2018
 */
@Service
public class AddressService {

    private AddressRepository addressRepository;
    private AddressConverter addressConverter;

    @Autowired
    public AddressService(AddressRepository addressRepository, AddressConverter addressConverter) {
        this.addressRepository = addressRepository;
        this.addressConverter = addressConverter;
    }

    public Integer insert(String city, String postalcode, String street, Integer number){
        AddressDTO addressDTO = new AddressDTO.Builder()
                .setCity(city)
                .setPostalcode(postalcode)
                .setStreet(street)
                .setNumber(number)
                .create();
        return addressRepository.save(addressConverter.toEntity(addressDTO)).getId();
    }

}
