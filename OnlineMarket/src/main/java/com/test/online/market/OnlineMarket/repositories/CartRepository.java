package com.test.online.market.OnlineMarket.repositories;

import com.test.online.market.OnlineMarket.dto.CartDTO;
import com.test.online.market.OnlineMarket.model.Cart;
import com.test.online.market.OnlineMarket.model.Order;
import com.test.online.market.OnlineMarket.model.Product;
import com.test.online.market.OnlineMarket.model.Restaurant;
import com.test.online.market.OnlineMarket.utils.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
public interface CartRepository extends JpaRepository<Cart,Integer>{
    List<Cart> findByOrder(Order order);

    @Transactional
    Integer deleteByProductAndOrder(Product product, Order order);

    @Transactional
    @Modifying
    @Query("UPDATE Cart c SET c.quantity=?1 WHERE c.order.id=?2 AND c.product.id = ?3")
    void updateStatus(Integer quantity, Integer orderId, Integer productId);
}
