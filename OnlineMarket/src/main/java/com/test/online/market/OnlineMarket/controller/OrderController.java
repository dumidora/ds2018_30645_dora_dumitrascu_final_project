package com.test.online.market.OnlineMarket.controller;

import com.test.online.market.OnlineMarket.dto.CartDTO;
import com.test.online.market.OnlineMarket.dto.ClientDTO;
import com.test.online.market.OnlineMarket.dto.OrderDTO;
import com.test.online.market.OnlineMarket.dto.OrderHistory;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.services.CartService;
import com.test.online.market.OnlineMarket.services.ClientService;
import com.test.online.market.OnlineMarket.services.OrderService;
import com.test.online.market.OnlineMarket.utils.DataUtil;
import com.test.online.market.OnlineMarket.utils.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
@RestController
@RequestMapping({"/api/order"})
public class OrderController {

    private CartService cartService;
    private OrderService orderService;
    private ClientService clientService;

    @Autowired
    public OrderController(CartService cartService, OrderService orderService,ClientService clientService) {
        this.cartService = cartService;
        this.orderService = orderService;
        this.clientService = clientService;
    }

    @PostMapping("/checkout")
    public ResponseEntity validateOrder(Integer orderId){
        try{
            List<CartDTO> cartDTOS = cartService.getCartByOrder(orderId);
            if(cartDTOS.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            boolean ok = cartService.isCartValid(orderId);
            if(!ok){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
            else{
                orderService.updateStatus(OrderStatus.SENT,orderId);
                return new ResponseEntity(HttpStatus.OK);
            }
        } catch (ConverterException e) {
            e.printStackTrace();
        }
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

//    @RequestMapping(value = "/history/{username}", method = RequestMethod.GET)
//    public String orderHistory(Model model, @PathVariable String username){
//        try{
//            ClientDTO clientDTO = clientService.getByUsername(username);
//            List<OrderDTO> orderDTOs = orderService.getClientHistory(clientDTO);
//            HashMap<OrderDTO,List<CartDTO>> myMap = new HashMap<>();
//
//            ArrayList<Float> prices = new ArrayList<>();
//            for(OrderDTO orderDTO : orderDTOs){
//                List<CartDTO> cartDTOS = cartService.getCartByOrder(orderDTO.getId());
//                myMap.put(orderDTO,cartDTOS);
//            }
//
//            model.addAttribute("myMap",myMap);
//            model.addAttribute("username", DataUtil.convertEmail(clientDTO.getEmail()));
//        } catch (ConverterException e) {
//            e.printStackTrace();
//        }
//        return "history";
//    }

    @GetMapping("/history/{email}")
    public List<OrderHistory> getOrderHistory(@PathVariable String email) {
        try{
            ClientDTO clientDTO = clientService.getByEmail(email);
            List<OrderDTO> orderDTOs = orderService.getClientHistory(clientDTO);
            List<OrderHistory> result = new ArrayList<>();

            for(OrderDTO orderDTO : orderDTOs){
                List<CartDTO> cartDTOS = cartService.getCartByOrder(orderDTO.getId());
                Float total = cartService.getTotalPrice(cartDTOS);
                result.add(new OrderHistory(orderDTO, cartDTOS, total));
            }

            return result;

        } catch (ConverterException e) {
            e.printStackTrace();
        }
        return null;
    }
}
