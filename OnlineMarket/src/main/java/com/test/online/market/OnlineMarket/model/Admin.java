package com.test.online.market.OnlineMarket.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Dora on November, 2018
 *
 * class that represents the administrator of the database a.k.a. me
 */

@Entity
@Table(name = "admin")
@Getter
@Setter
@ToString(includeFieldNames = true)
@NoArgsConstructor
@AllArgsConstructor
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /**
     * usernames for admins will start with a_ to be able to identify who logs in
     */
    @Column(name = "username", unique = true, nullable = false, length = 40)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;
}
