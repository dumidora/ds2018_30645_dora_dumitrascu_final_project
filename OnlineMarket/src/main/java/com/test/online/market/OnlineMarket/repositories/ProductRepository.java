package com.test.online.market.OnlineMarket.repositories;

import com.test.online.market.OnlineMarket.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
public interface ProductRepository extends JpaRepository<Product,Integer>{

    /**
     * This query will return the products for a certain restaurant
     * and the price for each.
     * @return List<Object[Product,Float]>
     */
    @Query("SELECT p, mi.price FROM Product p, Restaurant r, MenuItem mi " +
            "WHERE r.id =?1 and r.id = mi.restaurant.id " +
            "and mi.product.id = p.id")
    List<Object[]> getRestaurantProducts(Integer restaurantId);

    /**
     * Query that searches the price for the given product in the
     * restaurant that was chosen
     * @return Float
     */
    @Query("SELECT mi.price FROM MenuItem mi, Cart c WHERE " +
            "c.product.id = mi.product.id AND " +
            "c.restaurant.id = mi.restaurant.id AND " +
            "c.product.id=?1 AND c.restaurant.id=?2")
    List<Float> getProductPrice(Integer productId, Integer restaurantId);

}
