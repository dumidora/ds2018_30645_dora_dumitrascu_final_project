package com.test.online.market.OnlineMarket.dto;

/**
 * Created by Dumitrascu Dora on Dec, 2018
 */
public class CartProductDTO {
    private String email;
    private Integer quantity;
    private String restaurantName;
    private Integer productId;

    public CartProductDTO(){}

    public CartProductDTO(String email, Integer quantity, String restaurantName, Integer productId) {
        this.email = email;
        this.quantity = quantity;
        this.restaurantName = restaurantName;
        this.productId = productId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
}
