package com.test.online.market.OnlineMarket.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Dora on November, 2018
 */

@Controller
public class MainController {
    @RequestMapping(name = "/", method = RequestMethod.GET)
    public String main() {
        return "index";
    }
}
