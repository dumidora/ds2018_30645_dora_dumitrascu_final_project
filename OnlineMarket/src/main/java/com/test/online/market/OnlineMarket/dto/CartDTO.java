package com.test.online.market.OnlineMarket.dto;

import com.test.online.market.OnlineMarket.model.Order;
import com.test.online.market.OnlineMarket.model.Product;
import com.test.online.market.OnlineMarket.model.Restaurant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Dora on November, 2018
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CartDTO {
    private Integer quantity;
    private ProductDTO product;
    private Order order;
    private Restaurant restaurant;
    private Float price;

    public static class Builder{
        private Integer quantityBuilder;
        private ProductDTO productBuilder;
        private Order orderBuilder;
        private Restaurant restaurantBuilder;
        private Float priceBuilder;

        public Builder setQuantity(Integer quantityBuilder){
            this.quantityBuilder = quantityBuilder;
            return this;
        }

        public Builder setProduct(ProductDTO productBuilder){
            this.productBuilder = productBuilder;
            return this;
        }

        public Builder setOrder(Order orderBuilder){
            this.orderBuilder = orderBuilder;
            return this;
        }

        public Builder setRestaurant(Restaurant restaurantBuilder){
            this.restaurantBuilder = restaurantBuilder;
            return this;
        }

        public Builder setPrice(Float priceBuilder) {
            this.priceBuilder = priceBuilder;
            return this;
        }

        public CartDTO create(){
            return new CartDTO(quantityBuilder,productBuilder,orderBuilder,restaurantBuilder, priceBuilder);
        }
    }
}
