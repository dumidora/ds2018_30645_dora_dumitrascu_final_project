package com.test.online.market.OnlineMarket.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Dora on November, 2018
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO {

    private String city;
    private String postalcode;
    private String street;
    private Integer number;

    public static class Builder{
        private String cityBuilder;
        private String postalcodeBuilder;
        private String streetBuilder;
        private Integer numberBuilder;

        public Builder setCity(String cityBuilder){
            this.cityBuilder = cityBuilder;
            return this;
        }

        public Builder setPostalcode(String postalcodeBuilder){
            this.postalcodeBuilder = postalcodeBuilder;
            return this;
        }

        public Builder setStreet(String streetBuilder){
            this.streetBuilder = streetBuilder;
            return this;
        }

        public Builder setNumber(Integer numberBuilder){
            this.numberBuilder = numberBuilder;
            return this;
        }

        public AddressDTO create(){
            return new AddressDTO(cityBuilder,postalcodeBuilder,streetBuilder,numberBuilder);
        }
    }
}
