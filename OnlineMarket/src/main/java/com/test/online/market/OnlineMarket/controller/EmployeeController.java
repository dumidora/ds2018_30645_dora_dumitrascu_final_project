package com.test.online.market.OnlineMarket.controller;

import com.test.online.market.OnlineMarket.dto.*;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Employee;
import com.test.online.market.OnlineMarket.model.LoginModel;
import com.test.online.market.OnlineMarket.model.Order;
import com.test.online.market.OnlineMarket.services.CartService;
import com.test.online.market.OnlineMarket.services.EmployeeService;
import com.test.online.market.OnlineMarket.services.OrderService;
import com.test.online.market.OnlineMarket.utils.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
@RestController
@RequestMapping({"/api/employee"})
public class EmployeeController {
    private EmployeeService employeeService;
    private OrderService orderService;
    private CartService cartService;

    @Autowired
    public EmployeeController(CartService cartService , EmployeeService employeeService, OrderService orderService) {
        this.employeeService = employeeService;
        this.orderService = orderService;
        this.cartService = cartService;
    }

    @RequestMapping(value = "/employeeLogin",method = RequestMethod.GET)
    public String loginEmployee(){
        return "employeeLogin";
    }

    @PostMapping(value = "/login", consumes = "application/json")
    @CrossOrigin(origins = "http://localhost:63342")
    public ResponseEntity<EmployeeDTO> login(@RequestBody LoginModel login) {
        try {
            EmployeeDTO employee = employeeService.login(login.email, login.password);
            return new ResponseEntity<>(employee, HttpStatus.OK);
        } catch(ConverterException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/home")
    public List<OrderHistory> employeeHome(String username) throws ConverterException {
        EmployeeDTO employeeDTO = employeeService.findByUsername(username);

        //select orders that were sent
        List<OrderDTO> sentOrders = orderService.getAll()
                .stream()
                .filter(order -> order.getStatus().equals(OrderStatus.SENT))
                .collect(Collectors.toList());
        //map all orders with the list of products
        List<OrderHistory> myMap = new ArrayList<>();
        //select orders for the employee's restaurant
        List<CartDTO> employeeOrders;
        for(OrderDTO orderDTO : sentOrders){
            List<CartDTO> cartDTOS= cartService.getCartByOrder(orderDTO.getId());
            employeeOrders = new ArrayList<>();
            for(CartDTO cartDTO :cartDTOS){
                if(cartDTO.getRestaurant().getId().equals(employeeDTO.getRestaurant().getId())){
                    employeeOrders.add(cartDTO);
                }
            }

            Float totalPrice = cartService.getTotalPrice(employeeOrders);
            if(!employeeOrders.isEmpty()) myMap.add(new OrderHistory(orderDTO, employeeOrders, totalPrice));
        }
        return myMap;
    }

    @PostMapping("/confirmOrder/{orderId}")
    public void confirmOrder(@PathVariable Integer orderId) {
        orderService.updateStatus(OrderStatus.SHIPPING, orderId);
    }
}
