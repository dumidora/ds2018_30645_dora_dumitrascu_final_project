package com.test.online.market.OnlineMarket.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.test.online.market.OnlineMarket.utils.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */
@Entity
@Table(name = "order_table")
@Getter
@Service
@ToString(includeFieldNames = true)
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    private List<Cart> carts = new ArrayList<>();

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @Temporal(TemporalType.DATE)
    private Date placementDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "varchar(255) default 'PENDING'")
    private OrderStatus status;

    public Order(List<Cart> carts, Client client, Date placementDate, OrderStatus status) {
        this.carts = carts;
        this.client = client;
        this.placementDate = placementDate;
        this.status = status;
    }
}
