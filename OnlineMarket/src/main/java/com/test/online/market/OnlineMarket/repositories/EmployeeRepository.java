package com.test.online.market.OnlineMarket.repositories;

import com.test.online.market.OnlineMarket.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
    Employee findByUsername(String username);
    Employee findByUsernameAndPassword(String username, String password);
}
