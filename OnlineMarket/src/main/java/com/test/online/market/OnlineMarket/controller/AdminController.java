package com.test.online.market.OnlineMarket.controller;

import com.test.online.market.OnlineMarket.dto.RestaurantProduct;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Admin;
import com.test.online.market.OnlineMarket.model.LoginModel;
import com.test.online.market.OnlineMarket.model.Product;
import com.test.online.market.OnlineMarket.repositories.AdminRepository;
import com.test.online.market.OnlineMarket.services.ClientService;
import com.test.online.market.OnlineMarket.services.ProductService;
import com.test.online.market.OnlineMarket.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Dumitrascu Dora on Jan, 2019
 */
@CrossOrigin(origins = "http://localhost:63342")
@RestController
@RequestMapping({"/api/admin"})
public class AdminController {
    private AdminRepository adminRepository;
    private ClientService clientService;
    private RestaurantService restaurantService;
    private ProductService productService;

    @Autowired
    public AdminController(AdminRepository adminRepository, ClientService clientService, RestaurantService restaurantService, ProductService productService) {
        this.adminRepository = adminRepository;
        this.clientService = clientService;
        this.restaurantService = restaurantService;
        this.productService = productService;
    }

    @PostMapping(value = "/login", consumes = "application/json")
    @CrossOrigin(origins = "http://localhost:63342")
    public ResponseEntity<Admin> login(@RequestBody LoginModel login) {
        Admin user = adminRepository.findByUsernameAndPassword(login.email, login.password);
        if(user == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping("/client/delete/{email}")
    public void deleteClient(@PathVariable String email) {
        clientService.deleteByEmail(email);
    }

    @PostMapping("/restaurant/delete/{id}")
    public void deleteRestaurant(@PathVariable Integer id) {
        restaurantService.removeById(id);
    }

    @GetMapping("/products/all")
    public List<Product> findAllProducts() {
        return productService.findAll();
    }

    @PostMapping("/addProduct")
    public void addProduct(@RequestBody RestaurantProduct prod) throws ConverterException {
        System.out.println(prod.toString());
    }

}
