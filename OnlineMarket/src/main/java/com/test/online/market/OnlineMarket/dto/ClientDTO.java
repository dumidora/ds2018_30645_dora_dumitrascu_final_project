package com.test.online.market.OnlineMarket.dto;

import com.test.online.market.OnlineMarket.model.Address;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Dora on November, 2018
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO {
    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private String telephone;
    private Address address;

    public static class Builder{
        private String firstnameBuilder;
        private String lastnameBuilder;
        private String emailBuilder;
        private String passwordBuilder;
        private String telephoneBuilder;
        private Address addressBuilder;

        public Builder setFirstname(String firstnameBuilder){
            this.firstnameBuilder = firstnameBuilder;
            return this;
        }

        public Builder setLastname(String lastnameBuilder){
            this.lastnameBuilder = lastnameBuilder;
            return this;
        }

        public Builder setEmail(String emailBuilder){
            this.emailBuilder = emailBuilder;
            return this;
        }

        public Builder setPassword(String passwordBuilder){
            this.passwordBuilder = passwordBuilder;
            return this;
        }

        public Builder setTelephone(String telephoneBuilder){
            this.telephoneBuilder = telephoneBuilder;
            return this;
        }

        public Builder setAddress(Address addressBuilder){
            this.addressBuilder = addressBuilder;
            return this;
        }

        public ClientDTO create(){
            return new ClientDTO(firstnameBuilder,lastnameBuilder,emailBuilder,passwordBuilder,telephoneBuilder,addressBuilder);
        }
    }
}
