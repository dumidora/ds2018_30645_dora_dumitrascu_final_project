package com.test.online.market.OnlineMarket.dto;

import com.test.online.market.OnlineMarket.model.Client;
import com.test.online.market.OnlineMarket.utils.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Created by Dora on November, 2018
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {
    private Integer id;
    private Date date;
    private OrderStatus status;
    private Client client;

    public static class Builder{
        private Integer idBuilder;
        private Date dateBuilder;
        private OrderStatus statusBuilder;
        private Client clientBuilder;

        public Builder setId(Integer idBuilder){
            this.idBuilder = idBuilder;
            return this;
        }

        public Builder setDate(Date dateBuilder){
            this.dateBuilder = dateBuilder;
            return this;
        }

        public Builder setStatus(OrderStatus statusBuilder){
            this.statusBuilder = statusBuilder;
            return this;
        }

        public Builder setClient(Client clientBuilder){
            this.clientBuilder = clientBuilder;
            return this;
        }

        public OrderDTO create(){
            return new OrderDTO(idBuilder,dateBuilder,statusBuilder,clientBuilder);
        }
    }
}
