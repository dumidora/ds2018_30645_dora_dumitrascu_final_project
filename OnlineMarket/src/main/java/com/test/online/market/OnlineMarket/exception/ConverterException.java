package com.test.online.market.OnlineMarket.exception;

import ch.qos.logback.core.encoder.EchoEncoder;

/**
 * Created by Dora on November, 2018
 */
public class ConverterException extends Exception{
    public ConverterException(){super();}
    public ConverterException(String message){ super(message);}
}
