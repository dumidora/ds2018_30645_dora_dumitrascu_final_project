package com.test.online.market.OnlineMarket.dto;

/**
 * Created by Dora on November, 2018
 */

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * class for implementing the Iterator Design patter
 */
public class ProductDTOBox {
    private final List<ProductDTO> list = new ArrayList<>();

    public class Iterator{
        private ProductDTOBox box;
        private java.util.Iterator iterator;
        private ProductDTO value;

        public Iterator(ProductDTOBox productDTOBox){
            box = productDTOBox;
        }

        public void first(){
            iterator = box.list.iterator();
            next();
        }

        public void next(){
            try {
                value = (ProductDTO) iterator.next();
            }catch(NoSuchElementException ex){
                value = null;
            }
        }

        public boolean isDone(){
            return value == null;
        }

        public ProductDTO currentValue(){
            return value;
        }
    }


    public void add(ProductDTO in){
        list.add(in);
    }

    public Iterator getIterator(){
        return new Iterator(this);
    }
}
