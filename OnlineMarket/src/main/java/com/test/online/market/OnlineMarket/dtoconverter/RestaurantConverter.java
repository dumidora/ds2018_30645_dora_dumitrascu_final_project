package com.test.online.market.OnlineMarket.dtoconverter;

import com.test.online.market.OnlineMarket.dto.RestaurantDTO;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Restaurant;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Dora on November, 2018
 */
@Component
public class RestaurantConverter implements Converter<Restaurant,RestaurantDTO>{
    @Override
    public RestaurantDTO toDTO(Restaurant restaurant) throws ConverterException {
        if(restaurant!= null)
            return new RestaurantDTO.Builder()
            .setId(restaurant.getId())
            .setName(restaurant.getName())
            .setEmployee(restaurant.getEmployee())
            .setMenuItems(restaurant.getMenuItems())
            .setImage(restaurant.getImage())
            .create();
        else
            throw new ConverterException("Converter exception! Restaurant not found!");
    }

    @Override
    public List<RestaurantDTO> toDTOs(List<Restaurant> objects) throws ConverterException {
        List<RestaurantDTO> restaurantDTOS = null;
        for(Restaurant restaurant: objects){
            restaurantDTOS.add(toDTO(restaurant));
        }
        return restaurantDTOS;
    }

    @Override
    public Restaurant toEntity(RestaurantDTO object) {
        return null;
    }

    @Override
    public List<Restaurant> toEntities(List<RestaurantDTO> objects) {
        return null;
    }
}
