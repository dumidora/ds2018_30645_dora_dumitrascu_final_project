package com.test.online.market.OnlineMarket.dtoconverter;

import com.test.online.market.OnlineMarket.dto.ProductDTO;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */
@Component
public class ProductConverter implements Converter<Product,ProductDTO>{
    @Override
    public ProductDTO toDTO(Product object) throws ConverterException {
        if(object!=null)
            return new ProductDTO.Builder()
            .setId(object.getId())
            .setName(object.getName())
            .setCategory(object.getCategory())
            .setDescription(object.getDescription())
            .create();
        else
            throw new ConverterException("Product is null!");
    }

    @Override
    public List<ProductDTO> toDTOs(List<Product> objects) throws ConverterException {
        List<ProductDTO> result = new ArrayList<>();
        for(Product product : objects){
            result.add(toDTO(product));
        }
        return result;
    }

    @Override
    public Product toEntity(ProductDTO object) {
        return new Product(object.getId(),object.getName(),object.getDescription(),object.getCategory());
    }

    @Override
    public List<Product> toEntities(List<ProductDTO> objects) {
        List<Product> result = new ArrayList<>();
        for(ProductDTO productDTO : objects){
            result.add(toEntity(productDTO));
        }
        return result;
    }
}
