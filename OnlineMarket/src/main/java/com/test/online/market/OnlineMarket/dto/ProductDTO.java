package com.test.online.market.OnlineMarket.dto;

import com.test.online.market.OnlineMarket.utils.ProductCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Dora on November, 2018
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {
    private Integer id;
    private String name;
    private String description;
    private ProductCategory category;

    public static class Builder{
        private Integer idBuilder;
        private String nameBuilder;
        private String descriptionBuilder;
        private ProductCategory categoryBuilder;

        public Builder setId(Integer idBuilder){
            this.idBuilder = idBuilder;
            return this;
        }

        public Builder setName(String nameBuilder){
            this.nameBuilder = nameBuilder;
            return this;
        }

        public Builder setDescription(String descriptionBuilder){
            this.descriptionBuilder = descriptionBuilder;
            return this;
        }

        public Builder setCategory(ProductCategory categoryBuilder){
            this.categoryBuilder= categoryBuilder;
            return this;
        }

        public ProductDTO create(){
            return new ProductDTO(idBuilder,nameBuilder,descriptionBuilder,categoryBuilder);
        }
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category=" + category +
                '}';
    }
}
