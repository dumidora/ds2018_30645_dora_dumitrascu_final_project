package com.test.online.market.OnlineMarket.dto;

import com.test.online.market.OnlineMarket.model.Restaurant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Dora on November, 2018
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDTO {
    private String username;
    private String password;
    private Restaurant restaurant;

    public static class Builder{
        private String usernameBuilder;
        private String passwordBuilder;
        private Restaurant restaurantBuilder;

        public Builder setUsername(String usernameBuilder){
            this.usernameBuilder = usernameBuilder;
            return this;
        }

        public Builder setPassword(String passwordBuilder){
            this.passwordBuilder = passwordBuilder;
            return this;
        }

        public Builder setRestaurant(Restaurant restaurantBuilder){
            this.restaurantBuilder = restaurantBuilder;
            return this;
        }

        public EmployeeDTO create(){
            return new EmployeeDTO(usernameBuilder,passwordBuilder,restaurantBuilder);
        }
    }
}
