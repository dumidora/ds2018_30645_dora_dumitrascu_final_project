package com.test.online.market.OnlineMarket.dtoconverter;

import com.test.online.market.OnlineMarket.dto.OrderDTO;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@Component
public class OrderConverter implements Converter<Order,OrderDTO> {
    @Override
    public OrderDTO toDTO(Order object) throws ConverterException {
        if(object!=null)
            return new OrderDTO.Builder()
                    .setId(object.getId())
                    .setDate(object.getPlacementDate())
                    .setStatus(object.getStatus())
                    .setClient(object.getClient())
                    .create();
        else
            throw new ConverterException("Order is null!");
    }

    @Override
    public List<OrderDTO> toDTOs(List<Order> objects) throws ConverterException {
        List<OrderDTO> orderDTOS = new ArrayList<>();
        for(Order order: objects){
            orderDTOS.add(toDTO(order));
        }
        return orderDTOS;
    }

    @Override
    public Order toEntity(OrderDTO object) {
        return null;
    }

    @Override
    public List<Order> toEntities(List<OrderDTO> objects) {
        return null;
    }
}