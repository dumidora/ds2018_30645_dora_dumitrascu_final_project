package com.test.online.market.OnlineMarket.utils;

/**
 * Created by Dora on November, 2018
 */
public enum ProductCategory {
    INTERNATIONAL, EUROPEAN, ITALIAN, BURGERS, VEGAN, MEDITERRANEAN, SANDWICHES,
    ASIAN, PASTRIES, DESSERT, JAPANESE, PIZZA
}
