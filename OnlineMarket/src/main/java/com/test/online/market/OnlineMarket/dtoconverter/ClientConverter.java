package com.test.online.market.OnlineMarket.dtoconverter;

import com.test.online.market.OnlineMarket.dto.ClientDTO;
import com.test.online.market.OnlineMarket.model.Client;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@Component
public class ClientConverter implements Converter<Client,ClientDTO>{
    @Override
    public ClientDTO toDTO(Client client) throws ConverterException {
        if(client!=null)
            return new ClientDTO.Builder()
            .setFirstname(client.getFirstname())
            .setLastname(client.getLastname())
            .setEmail(client.getEmail())
            .setPassword(client.getPassword())
            .setTelephone(client.getTelephone())
            .setAddress(client.getAddress())
            .create();
        else
            throw new ConverterException("User not in database!");
    }

    @Override
    public List<ClientDTO> toDTOs(List<Client> objects) throws ConverterException {
        List<ClientDTO> clientDTOS = new ArrayList<>();

        for(Client client: objects){
            clientDTOS.add(toDTO(client));
        }

        return clientDTOS;
    }

    @Override
    public Client toEntity(ClientDTO object) {
        return new Client(object.getFirstname(),object.getLastname(),object.getEmail(),object.getPassword(),object.getTelephone(),object.getAddress());
    }

    @Override
    public List<Client> toEntities(List<ClientDTO> objects) {
        List<Client> clients = new ArrayList<>();

        for(ClientDTO clientDTO : objects){
            clients.add(toEntity(clientDTO));
        }
        return clients;
    }
}
