package com.test.online.market.OnlineMarket.controller;

import com.test.online.market.OnlineMarket.dto.AddressDTO;
import com.test.online.market.OnlineMarket.dto.ClientDTO;
import com.test.online.market.OnlineMarket.dto.RestaurantDTO;
import com.test.online.market.OnlineMarket.exception.ConverterException;

import com.test.online.market.OnlineMarket.model.Address;
import com.test.online.market.OnlineMarket.model.Client;
import com.test.online.market.OnlineMarket.model.LoginModel;
import com.test.online.market.OnlineMarket.services.AddressService;
import com.test.online.market.OnlineMarket.services.ClientService;
import com.test.online.market.OnlineMarket.services.OrderService;
import com.test.online.market.OnlineMarket.services.RestaurantService;
import com.test.online.market.OnlineMarket.utils.DataUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Convert;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
@RestController
@RequestMapping({"/api/client"})
public class ClientController {
    private ClientService clientService;
    private RestaurantService restaurantService;
    private AddressService addressService;
    private OrderService orderService;

    @Autowired
    public ClientController(ClientService clientService, RestaurantService restaurantService, AddressService addressService, OrderService orderService) {
        this.clientService = clientService;
        this.restaurantService = restaurantService;
        this.addressService = addressService;
        this.orderService = orderService;
    }

    @PostMapping("/persistClient")
    public ResponseEntity<String> persistClient(@RequestBody ClientDTO client) {
        clientService.insert(client.getFirstname(), client.getLastname(), client.getEmail(), client.getPassword(),client.getTelephone(), client.getAddress().getId());
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/users")
    public Collection<ClientDTO> getClients() {
        return clientService.getAll().stream()
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/login", consumes = "application/json")
    @CrossOrigin(origins = "http://localhost:63342")
    public ResponseEntity<ClientDTO> login(@RequestBody LoginModel login) {
        System.out.print(login.email + " " + login.password);
        try {
            ClientDTO user = clientService.login(login.email, login.password);
            orderService.createInitialOrder(user);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch(ConverterException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value = "/register", consumes = "application/json")
    public void register(@RequestBody ClientDTO user) {
        Address newAddress = user.getAddress();
        //create address then add user
        Integer insertedId = addressService.insert(
                newAddress.getCity(),
                newAddress.getPostalcode(),
                newAddress.getStreet(),
                newAddress.getNumber());
        clientService.insert(
                user.getFirstname(),
                user.getLastname(),
                user.getEmail(),
                user.getPassword(),
                user.getTelephone(),
                insertedId);
    }

    @GetMapping(value = "/home")
    public Collection<RestaurantDTO> homePage() {
        try {
            return restaurantService.getAll()
                    .stream()
                    .collect(Collectors.toList());
        } catch (ConverterException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/profile/{username}", method = RequestMethod.GET)
    public String profile(Model model, @PathVariable("username") String username) throws Exception {
        //add @% to partially match string in query -> see ClientRepository
        ClientDTO clientDTO = clientService.getByUsername(username);
        model.addAttribute("currentUser",clientDTO);
        model.addAttribute("username",username);
        return "profile";
    }
}
