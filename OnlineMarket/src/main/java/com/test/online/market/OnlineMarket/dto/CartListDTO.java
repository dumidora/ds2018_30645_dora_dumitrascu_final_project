package com.test.online.market.OnlineMarket.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Dumitrascu Dora on Jan, 2019
 */
@Getter
@Setter
public class CartListDTO {
    private Collection<CartDTO> carts;
    private Float totalPrice;

    public CartListDTO(Collection<CartDTO> carts, Float totalPrice) {
        this.carts = carts;
        this.totalPrice = totalPrice;
    }
}
