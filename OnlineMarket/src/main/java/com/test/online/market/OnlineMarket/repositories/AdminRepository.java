package com.test.online.market.OnlineMarket.repositories;

import com.test.online.market.OnlineMarket.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Created by Dumitrascu Dora on Jan, 2019
 */
@CrossOrigin(origins = "http://localhost:63342")
public interface AdminRepository extends JpaRepository<Admin, Integer> {
    Admin findByUsernameAndPassword(String username, String password);
}
