package com.test.online.market.OnlineMarket.dtoconverter;

import com.test.online.market.OnlineMarket.dto.CartDTO;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Cart;
import com.test.online.market.OnlineMarket.model.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@Component
public class CartConverter implements Converter<Cart,CartDTO>{
    @Override
    public CartDTO toDTO(Cart cart) throws ConverterException {
        ProductConverter productConverter = new ProductConverter();
        if(cart!=null)
            return new CartDTO.Builder()
            .setQuantity(cart.getQuantity())
            .setProduct(productConverter.toDTO(cart.getProduct()))
            .setOrder(cart.getOrder())
            .setRestaurant(cart.getRestaurant())
            .create();
        else
            throw new ConverterException("Cart not OK!");
    }

    @Override
    public List<CartDTO> toDTOs(List<Cart> objects) throws ConverterException {
        List<CartDTO> cartDTOS = new ArrayList<>();
        for(Cart cart : objects){
            cartDTOS.add(toDTO(cart));
        }
        return cartDTOS;
    }

    @Override
    public Cart toEntity(CartDTO cart) {
        ProductConverter converter = new ProductConverter();
        Product prod = converter.toEntity(cart.getProduct());
        return new Cart(cart.getQuantity(),prod,cart.getOrder(),cart.getRestaurant());
    }

    @Override
    public List<Cart> toEntities(List<CartDTO> objects) {
        List<Cart> carts = new ArrayList<>();
        for(CartDTO cartDTO : objects){
            carts.add(toEntity(cartDTO));
        }
        return carts;
    }
}
