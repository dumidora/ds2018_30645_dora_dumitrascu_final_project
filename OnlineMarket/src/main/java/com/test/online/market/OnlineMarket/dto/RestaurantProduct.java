package com.test.online.market.OnlineMarket.dto;

import lombok.*;

/**
 * Created by Dumitrascu Dora on Jan, 2019
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RestaurantProduct {
    private Integer productId;
    private Integer restaurantId;
    private Float price;
}
