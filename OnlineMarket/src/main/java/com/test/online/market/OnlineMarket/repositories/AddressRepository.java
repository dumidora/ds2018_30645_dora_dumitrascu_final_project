package com.test.online.market.OnlineMarket.repositories;

import com.test.online.market.OnlineMarket.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
public interface AddressRepository extends JpaRepository<Address,Integer>{
}
