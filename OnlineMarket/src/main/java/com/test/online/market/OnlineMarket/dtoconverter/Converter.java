package com.test.online.market.OnlineMarket.dtoconverter;

/**
 * Created by Dora on November, 2018
 */

import com.test.online.market.OnlineMarket.exception.ConverterException;

import java.util.List;

/**
 * this interface contains methods that are necessary to convert data from
 * the database so we don't handle it directly
 * @param <T> is the entity object retrieved from the database
 * @param <T2> is the DTO which the service will use
 */
public interface Converter<T,T2> {
    T2 toDTO(T object) throws ConverterException;

    List<T2> toDTOs(List<T> objects) throws ConverterException;

    T toEntity(T2 object);

    List<T> toEntities(List<T2> objects);
}