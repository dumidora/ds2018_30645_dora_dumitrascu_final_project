package com.test.online.market.OnlineMarket.services;

import com.test.online.market.OnlineMarket.dto.CartDTO;
import com.test.online.market.OnlineMarket.dto.ClientDTO;
import com.test.online.market.OnlineMarket.dto.OrderDTO;
import com.test.online.market.OnlineMarket.dtoconverter.ClientConverter;
import com.test.online.market.OnlineMarket.dtoconverter.OrderConverter;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Client;
import com.test.online.market.OnlineMarket.model.Order;
import com.test.online.market.OnlineMarket.repositories.ClientRepository;
import com.test.online.market.OnlineMarket.repositories.OrderRepository;
import com.test.online.market.OnlineMarket.utils.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Dora on November, 2018
 */

@Service
public class OrderService {
    private OrderRepository orderRepository;
    private OrderConverter orderConverter;
    private ClientConverter clientConverter;
    private ClientRepository clientRepository;


    @Autowired
    public OrderService(OrderRepository orderRepository, OrderConverter orderConverter, ClientConverter clientConverter, ClientRepository clientRepository) {
        this.orderRepository = orderRepository;
        this.orderConverter = orderConverter;
        this.clientConverter = clientConverter;
        this.clientRepository = clientRepository;
    }

    public Integer getMaxId(){
        return orderRepository.getMaxId();
    }

    public void createInitialOrder(ClientDTO clientDTO){
        //compute today's date
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dateSample = new Date();

        //dateFormat.format(dateSample); -> STRING
        String dateString = dateFormat.format(dateSample);
        String[] tokens = dateString.split("-");

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(tokens[0]));
        cal.set(Calendar.MONTH,Integer.parseInt(tokens[1]));
        cal.set(Calendar.DAY_OF_MONTH,Integer.parseInt(tokens[2]));
        Date date = cal.getTime();

        Client client = clientRepository.findByEmail(clientDTO.getEmail());
        Order order = new Order(new ArrayList<>(),client,date, OrderStatus.PENDING);
        orderRepository.save(order);
    }

    public OrderDTO findById(Integer id) throws ConverterException {
        return orderConverter.toDTO(orderRepository.findById(id).get());
    }

    public void updateStatus(OrderStatus orderStatus, Integer orderId){
        orderRepository.updateStatus(orderStatus,orderId);
    }

    public List<OrderDTO> getAll() throws ConverterException {
        return orderConverter.toDTOs(orderRepository.findAll());
    }

    public List<OrderDTO> getClientHistory(ClientDTO clientDTO) throws ConverterException {
        List<Order> orders = orderRepository.findAll()
                .stream()
                .filter(o->(o.getStatus().equals(OrderStatus.SENT) || o.getStatus().equals(OrderStatus.SHIPPING)) && o.getClient().getEmail().equals(clientDTO.getEmail()))
                .collect(Collectors.toList());
        return orderConverter.toDTOs(orders);
    }


}
