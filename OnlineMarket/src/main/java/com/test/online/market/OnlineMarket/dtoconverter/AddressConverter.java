package com.test.online.market.OnlineMarket.dtoconverter;

import com.test.online.market.OnlineMarket.dto.AddressDTO;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Address;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@Component
public class AddressConverter implements Converter<Address,AddressDTO>{
    @Override
    public AddressDTO toDTO(Address object) throws ConverterException {
        if(object!=null)
            return new AddressDTO.Builder()
            .setCity(object.getCity())
            .setStreet(object.getStreet())
            .setPostalcode(object.getPostalcode())
            .setNumber(object.getNumber())
            .create();
        else
            throw new ConverterException("Address is null!");
    }

    @Override
    public List<AddressDTO> toDTOs(List<Address> objects) throws ConverterException {
        List<AddressDTO> result = new ArrayList<>();
        for(Address address:objects){
            result.add(toDTO(address));
        }
        return result;
    }

    @Override
    public Address toEntity(AddressDTO object) {
        return new Address(object.getCity(),object.getPostalcode(),object.getStreet(),object.getNumber());
    }

    @Override
    public List<Address> toEntities(List<AddressDTO> objects) {
        List<Address> addresses = new ArrayList<>();
        for(AddressDTO addressDTO : objects){
            addresses.add(toEntity(addressDTO));
        }
        return addresses;
    }
}
