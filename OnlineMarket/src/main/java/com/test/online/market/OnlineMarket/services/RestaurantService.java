package com.test.online.market.OnlineMarket.services;

import com.test.online.market.OnlineMarket.dto.ProductDTO;
import com.test.online.market.OnlineMarket.dto.RestaurantDTO;
import com.test.online.market.OnlineMarket.dtoconverter.ProductConverter;
import com.test.online.market.OnlineMarket.dtoconverter.RestaurantConverter;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.model.Product;
import com.test.online.market.OnlineMarket.model.Restaurant;
import com.test.online.market.OnlineMarket.repositories.ProductRepository;
import com.test.online.market.OnlineMarket.repositories.RestaurantRepository;
import com.test.online.market.OnlineMarket.utils.ProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */
@Service
public class RestaurantService {
    private RestaurantRepository restaurantRepository;
    private RestaurantConverter restaurantConverter;
    private ProductRepository productRepository;
    private ProductConverter productConverter;

    @Autowired
    public RestaurantService(RestaurantRepository restaurantRepository, RestaurantConverter restaurantConverter, ProductRepository productRepository, ProductConverter productConverter) {
        this.restaurantRepository = restaurantRepository;
        this.restaurantConverter = restaurantConverter;
        this.productRepository = productRepository;
        this.productConverter = productConverter;
    }

    public List<RestaurantDTO> getAll() throws ConverterException {
        List<Restaurant> restaurants = restaurantRepository.findAll();
        List<RestaurantDTO> restaurantDTOS = new ArrayList<>();
        for(Restaurant restaurant: restaurants){
            restaurantDTOS.add(restaurantConverter.toDTO(restaurant));
        }
        return restaurantDTOS;
    }

    public List<ProductDTO> getRestaurantProducts(Integer restaurantId) throws ConverterException {
        List<Object[]> products = productRepository.getRestaurantProducts(restaurantId);
        List<ProductDTO> result = null;

        for(Object[] objects : products){
            result.add(productConverter.toDTO((Product) objects[0]));
        }
        return result;
    }

    public RestaurantDTO findByName(String name) throws ConverterException {
        return restaurantConverter.toDTO(restaurantRepository.findByName(name));
    }

    public Restaurant findById(Integer id) throws ConverterException {
        return restaurantRepository.findById(id).get();
    }

    public void removeById(Integer id) {
        restaurantRepository.deleteById(id);
    }
}
