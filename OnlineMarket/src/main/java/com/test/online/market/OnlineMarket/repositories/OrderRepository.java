package com.test.online.market.OnlineMarket.repositories;

import com.test.online.market.OnlineMarket.model.Order;
import com.test.online.market.OnlineMarket.utils.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Created by Dora on November, 2018
 */
@CrossOrigin(origins = "http://localhost:63342")
public interface OrderRepository extends JpaRepository<Order,Integer> {

    /**
     * This method returns the last id that was inserted, as it is auto-incremented.
     * See model class 'Order' for generation strategy.
     * WARNING: this can return NULL if there is no entry in the 'order_table' in the database!
     */
    @Query("SELECT MAX(o.id) FROM Order o")
    Integer getMaxId();

    /**
     * This method updates the status of an order, e.g. when an employee of a restaurant accepts it.
     * @param orderStatus the new status of the order
     * @param id the id of the order
     */
    @Transactional
    @Modifying
    @Query("UPDATE Order o SET o.status=?1 WHERE o.id=?2")
    void updateStatus(OrderStatus orderStatus, Integer id);
}
