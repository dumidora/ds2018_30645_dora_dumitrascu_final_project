package com.test.online.market.OnlineMarket.services;

import com.test.online.market.OnlineMarket.dto.EmployeeDTO;
import com.test.online.market.OnlineMarket.dtoconverter.EmployeeConverter;
import com.test.online.market.OnlineMarket.exception.ConverterException;
import com.test.online.market.OnlineMarket.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Dora on November, 2018
 */

@Service
public class EmployeeService {
    private EmployeeRepository employeeRepository;
    private EmployeeConverter employeeConverter;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, EmployeeConverter employeeConverter) {
        this.employeeRepository = employeeRepository;
        this.employeeConverter = employeeConverter;
    }

    public EmployeeDTO findById(Integer id) throws ConverterException {
        return employeeConverter.toDTO(employeeRepository.findById(id).get());
    }

    public EmployeeDTO findByUsername(String username) throws ConverterException{
        return employeeConverter.toDTO(employeeRepository.findByUsername(username));
    }

    public EmployeeDTO login(String username, String password) throws ConverterException {
        return employeeConverter.toDTO(employeeRepository.findByUsernameAndPassword(username, password));
    }
}
