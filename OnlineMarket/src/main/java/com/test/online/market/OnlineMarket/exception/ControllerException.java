package com.test.online.market.OnlineMarket.exception;

/**
 * Created by Dora on November, 2018
 */
public class ControllerException extends Exception{
    public ControllerException(){
        super();
    }

    public ControllerException(String message){
        super(message);
    }
}
