package com.test.online.market.OnlineMarket.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Dumitrascu Dora on Jan, 2019
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class OrderHistory {
    private OrderDTO order;
    private List<CartDTO> carts;
    private Float totalPrice;
}
