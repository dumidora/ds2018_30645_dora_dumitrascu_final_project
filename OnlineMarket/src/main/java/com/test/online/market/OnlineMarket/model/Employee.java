package com.test.online.market.OnlineMarket.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

/**
 * Created by Dora on November, 2018
 *
 * class representing the employee of a restaurant
 * this user will review and confirm the orders
 */


@Entity
@Table(name = "employee")
@Getter
@Setter
@ToString(exclude={"restaurant"})
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name = "id", unique =  true, nullable = false)
    private Integer id;


    /**
     * usernames for employees will start with e_name to be able to identify what type
     * of user logs in
     */
    @Column(name = "username", unique = true, nullable = false, length = 40)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @JsonIgnore
    @OneToOne(fetch  = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "employee")
    private Restaurant restaurant;
}
