package com.test.online.market.OnlineMarket.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.test.online.market.OnlineMarket.utils.ProductCategory;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dora on November, 2018
 */

@Entity
@Table(name = "product")
@Getter
@Setter
@ToString(includeFieldNames = true)
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "name", nullable = false, unique = true, length = 50)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "category", nullable = false)
    private ProductCategory category;

    @JsonIgnore
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<Cart> carts = new ArrayList<Cart>();

    @JsonIgnore
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<MenuItem> menuItems = new ArrayList<>();

    public Product(Integer id, String name, String description, ProductCategory category) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.category = category;
        this.carts = new ArrayList<>();
        this.menuItems = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category=" + category.name() +
                '}';
    }
}
